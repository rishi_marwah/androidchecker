.class public Lcom/ntu/converge/MainActivity;
.super Landroid/app/Activity;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ntu/converge/MainActivity$mylocationlistener;
    }
.end annotation


# static fields
.field private static gpsLocation:Lcom/parse/ParseGeoPoint;

.field private static userID:Ljava/lang/String;


# instance fields
.field private fbLogin:Lcom/facebook/widget/LoginButton;

.field private progressDialog:Landroid/app/Dialog;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/ntu/converge/MainActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/ntu/converge/MainActivity;->onLoginButtonClicked()V

    return-void
.end method

.method static synthetic access$1(Lcom/ntu/converge/MainActivity;)Landroid/app/Dialog;
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/ntu/converge/MainActivity;->progressDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$2(Lcom/ntu/converge/MainActivity;Lcom/parse/ParseObject;Lcom/parse/ParseException;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 159
    invoke-direct {p0, p1, p2}, Lcom/ntu/converge/MainActivity;->setUserDetails(Lcom/parse/ParseObject;Lcom/parse/ParseException;)V

    return-void
.end method

.method static synthetic access$3(Lcom/ntu/converge/MainActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/ntu/converge/MainActivity;->getUserId()V

    return-void
.end method

.method static synthetic access$4()Ljava/lang/String;
    .registers 1

    .prologue
    .line 33
    sget-object v0, Lcom/ntu/converge/MainActivity;->userID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5(Ljava/lang/String;)V
    .registers 1
    .parameter

    .prologue
    .line 33
    sput-object p0, Lcom/ntu/converge/MainActivity;->userID:Ljava/lang/String;

    return-void
.end method

.method public static getLocation()Lcom/parse/ParseGeoPoint;
    .registers 1

    .prologue
    .line 41
    sget-object v0, Lcom/ntu/converge/MainActivity;->gpsLocation:Lcom/parse/ParseGeoPoint;

    return-object v0
.end method

.method public static getUserID()Ljava/lang/String;
    .registers 1

    .prologue
    .line 37
    sget-object v0, Lcom/ntu/converge/MainActivity;->userID:Ljava/lang/String;

    return-object v0
.end method

.method private getUserId()V
    .registers 4

    .prologue
    .line 143
    invoke-static {}, Lcom/parse/ParseFacebookUtils;->getSession()Lcom/facebook/Session;

    move-result-object v1

    .line 144
    new-instance v2, Lcom/ntu/converge/MainActivity$3;

    invoke-direct {v2, p0}, Lcom/ntu/converge/MainActivity$3;-><init>(Lcom/ntu/converge/MainActivity;)V

    .line 143
    invoke-static {v1, v2}, Lcom/facebook/Request;->newMeRequest(Lcom/facebook/Session;Lcom/facebook/Request$GraphUserCallback;)Lcom/facebook/Request;

    move-result-object v0

    .line 157
    .local v0, request:Lcom/facebook/Request;
    invoke-virtual {v0}, Lcom/facebook/Request;->executeAsync()Lcom/facebook/RequestAsyncTask;

    .line 158
    return-void
.end method

.method private onLoginButtonClicked()V
    .registers 6

    .prologue
    const/4 v4, 0x1

    .line 87
    .line 88
    const-string v1, ""

    const-string v2, "Logging in..."

    .line 87
    invoke-static {p0, v1, v2, v4}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/ntu/converge/MainActivity;->progressDialog:Landroid/app/Dialog;

    .line 89
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "basic_info"

    aput-object v3, v1, v2

    const-string v2, "user_about_me"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    .line 90
    const-string v3, "user_friends"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "user_location"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 91
    .local v0, permissions:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Lcom/ntu/converge/MainActivity$2;

    invoke-direct {v1, p0}, Lcom/ntu/converge/MainActivity$2;-><init>(Lcom/ntu/converge/MainActivity;)V

    invoke-static {v0, p0, v1}, Lcom/parse/ParseFacebookUtils;->logIn(Ljava/util/Collection;Landroid/app/Activity;Lcom/parse/LogInCallback;)V

    .line 108
    return-void
.end method

.method private setUserDetails(Lcom/parse/ParseObject;Lcom/parse/ParseException;)V
    .registers 6
    .parameter "user"
    .parameter "err"

    .prologue
    .line 160
    invoke-static {}, Lcom/parse/ParseFacebookUtils;->getSession()Lcom/facebook/Session;

    move-result-object v1

    .line 161
    new-instance v2, Lcom/ntu/converge/MainActivity$4;

    invoke-direct {v2, p0}, Lcom/ntu/converge/MainActivity$4;-><init>(Lcom/ntu/converge/MainActivity;)V

    .line 160
    invoke-static {v1, v2}, Lcom/facebook/Request;->newMeRequest(Lcom/facebook/Session;Lcom/facebook/Request$GraphUserCallback;)Lcom/facebook/Request;

    move-result-object v0

    .line 223
    .local v0, request:Lcom/facebook/Request;
    invoke-virtual {v0}, Lcom/facebook/Request;->executeAsync()Lcom/facebook/RequestAsyncTask;

    .line 224
    return-void
.end method


# virtual methods
.method public getUserLocation()V
    .registers 10

    .prologue
    .line 111
    const-string v1, "location"

    invoke-virtual {p0, v1}, Lcom/ntu/converge/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 112
    .local v0, lm:Landroid/location/LocationManager;
    new-instance v5, Lcom/ntu/converge/MainActivity$mylocationlistener;

    const/4 v1, 0x0

    invoke-direct {v5, p0, v1}, Lcom/ntu/converge/MainActivity$mylocationlistener;-><init>(Lcom/ntu/converge/MainActivity;Lcom/ntu/converge/MainActivity$mylocationlistener;)V

    .line 113
    .local v5, ll:Landroid/location/LocationListener;
    const-string v1, "network"

    const-wide/16 v2, 0x0

    .line 114
    const/4 v4, 0x0

    .line 113
    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 116
    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v6

    .line 117
    .local v6, l:Landroid/location/Location;
    new-instance v1, Lcom/parse/ParseGeoPoint;

    invoke-virtual {v6}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    .line 118
    invoke-virtual {v6}, Landroid/location/Location;->getLongitude()D

    move-result-wide v7

    .line 117
    invoke-direct {v1, v2, v3, v7, v8}, Lcom/parse/ParseGeoPoint;-><init>(DD)V

    sput-object v1, Lcom/ntu/converge/MainActivity;->gpsLocation:Lcom/parse/ParseGeoPoint;

    .line 119
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 4
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 82
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 83
    invoke-static {p1, p2, p3}, Lcom/parse/ParseFacebookUtils;->finishAuthentication(IILandroid/content/Intent;)V

    .line 84
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter "savedInstanceState"

    .prologue
    .line 46
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    const-string v1, "m6zTkVhDB73TaEjClsMeT6TID7l3IqxUu9UzgBoJ"

    const-string v2, "36VxdP32ethLMGAIy3gpwq60rBMHYU17IfxukHqN"

    invoke-static {p0, v1, v2}, Lcom/parse/Parse;->initialize(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const-string v1, "1461246090758663"

    invoke-static {v1}, Lcom/parse/ParseFacebookUtils;->initialize(Ljava/lang/String;)V

    .line 51
    const v1, 0x7f030015

    invoke-virtual {p0, v1}, Lcom/ntu/converge/MainActivity;->setContentView(I)V

    .line 52
    invoke-virtual {p0}, Lcom/ntu/converge/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/parse/ParseAnalytics;->trackAppOpened(Landroid/content/Intent;)V

    .line 54
    const v1, 0x7f04002c

    invoke-virtual {p0, v1}, Lcom/ntu/converge/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/LoginButton;

    iput-object v1, p0, Lcom/ntu/converge/MainActivity;->fbLogin:Lcom/facebook/widget/LoginButton;

    .line 55
    iget-object v1, p0, Lcom/ntu/converge/MainActivity;->fbLogin:Lcom/facebook/widget/LoginButton;

    new-instance v2, Lcom/ntu/converge/MainActivity$1;

    invoke-direct {v2, p0}, Lcom/ntu/converge/MainActivity$1;-><init>(Lcom/ntu/converge/MainActivity;)V

    invoke-virtual {v1, v2}, Lcom/facebook/widget/LoginButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    invoke-static {}, Lcom/parse/ParseUser;->getCurrentUser()Lcom/parse/ParseUser;

    move-result-object v0

    .line 73
    .local v0, currentUser:Lcom/parse/ParseUser;
    if-eqz v0, :cond_43

    invoke-static {v0}, Lcom/parse/ParseFacebookUtils;->isLinked(Lcom/parse/ParseUser;)Z

    move-result v1

    if-eqz v1, :cond_43

    .line 75
    invoke-virtual {p0}, Lcom/ntu/converge/MainActivity;->getUserLocation()V

    .line 76
    invoke-direct {p0}, Lcom/ntu/converge/MainActivity;->getUserId()V

    .line 78
    :cond_43
    return-void
.end method
