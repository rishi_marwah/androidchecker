.class public Lcom/ntu/converge/TabsPagerAdapter;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "TabsPagerAdapter.java"


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentManager;)V
    .registers 2
    .parameter "fm"

    .prologue
    .line 10
    invoke-direct {p0, p1}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 11
    return-void
.end method


# virtual methods
.method public getCount()I
    .registers 2

    .prologue
    .line 31
    const/4 v0, 0x3

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .registers 3
    .parameter "index"

    .prologue
    .line 16
    packed-switch p1, :pswitch_data_18

    .line 25
    const/4 v0, 0x0

    :goto_4
    return-object v0

    .line 18
    :pswitch_5
    new-instance v0, Lcom/ntu/converge/FeedFragment;

    invoke-direct {v0}, Lcom/ntu/converge/FeedFragment;-><init>()V

    goto :goto_4

    .line 20
    :pswitch_b
    new-instance v0, Lcom/ntu/converge/ExploreFragment;

    invoke-direct {v0}, Lcom/ntu/converge/ExploreFragment;-><init>()V

    goto :goto_4

    .line 22
    :pswitch_11
    new-instance v0, Lcom/ntu/converge/ProfileFragment;

    invoke-direct {v0}, Lcom/ntu/converge/ProfileFragment;-><init>()V

    goto :goto_4

    .line 16
    nop

    :pswitch_data_18
    .packed-switch 0x0
        :pswitch_5
        :pswitch_b
        :pswitch_11
    .end packed-switch
.end method
