.class public Lcom/ntu/converge/ExploreFragment;
.super Landroid/support/v4/app/ListFragment;
.source "ExploreFragment.java"


# instance fields
.field user_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    return-void
.end method

.method private buildData()Ljava/util/ArrayList;
    .registers 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 34
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 36
    .local v5, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    const-string v9, "Activities"

    invoke-static {v9}, Lcom/parse/ParseQuery;->getQuery(Ljava/lang/String;)Lcom/parse/ParseQuery;

    move-result-object v1

    .line 39
    .local v1, activitiesQuery:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    const-string v9, "gps"

    invoke-static {}, Lcom/ntu/converge/MainActivity;->getLocation()Lcom/parse/ParseGeoPoint;

    move-result-object v10

    invoke-virtual {v1, v9, v10}, Lcom/parse/ParseQuery;->whereNear(Ljava/lang/String;Lcom/parse/ParseGeoPoint;)Lcom/parse/ParseQuery;

    .line 41
    const/4 v0, 0x0

    .line 43
    .local v0, activities:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    :try_start_15
    invoke-virtual {v1}, Lcom/parse/ParseQuery;->find()Ljava/util/List;

    move-result-object v0

    .line 44
    const-string v9, "act check"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2f
    .catch Lcom/parse/ParseException; {:try_start_15 .. :try_end_2f} :catch_6f

    .line 50
    :goto_2f
    const-string v9, "User_data"

    invoke-static {v9}, Lcom/parse/ParseQuery;->getQuery(Ljava/lang/String;)Lcom/parse/ParseQuery;

    move-result-object v7

    .line 51
    .local v7, userQuery:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    const-string v9, "facebookId"

    iget-object v10, p0, Lcom/ntu/converge/ExploreFragment;->user_id:Ljava/lang/String;

    invoke-virtual {v7, v9, v10}, Lcom/parse/ParseQuery;->whereEqualTo(Ljava/lang/String;Ljava/lang/Object;)Lcom/parse/ParseQuery;

    .line 52
    const/4 v8, 0x0

    .line 54
    .local v8, users:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    :try_start_3d
    invoke-virtual {v7}, Lcom/parse/ParseQuery;->find()Ljava/util/List;

    move-result-object v8

    .line 55
    const-string v9, "User check"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_57
    .catch Lcom/parse/ParseException; {:try_start_3d .. :try_end_57} :catch_74

    .line 62
    :goto_57
    const/4 v9, 0x0

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/parse/ParseObject;

    const-string v10, "name"

    invoke-virtual {v9, v10}, Lcom/parse/ParseObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 64
    .local v6, name:Ljava/lang/String;
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_68
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_79

    .line 69
    return-object v5

    .line 45
    .end local v6           #name:Ljava/lang/String;
    .end local v7           #userQuery:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    .end local v8           #users:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    :catch_6f
    move-exception v4

    .line 47
    .local v4, e:Lcom/parse/ParseException;
    invoke-virtual {v4}, Lcom/parse/ParseException;->printStackTrace()V

    goto :goto_2f

    .line 56
    .end local v4           #e:Lcom/parse/ParseException;
    .restart local v7       #userQuery:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    .restart local v8       #users:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    :catch_74
    move-exception v4

    .line 58
    .restart local v4       #e:Lcom/parse/ParseException;
    invoke-virtual {v4}, Lcom/parse/ParseException;->printStackTrace()V

    goto :goto_57

    .line 64
    .end local v4           #e:Lcom/parse/ParseException;
    .restart local v6       #name:Ljava/lang/String;
    :cond_79
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/parse/ParseObject;

    .line 65
    .local v2, activity:Lcom/parse/ParseObject;
    const-string v10, "date"

    invoke-virtual {v2, v10}, Lcom/parse/ParseObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Date;

    .line 66
    .local v3, date:Ljava/util/Date;
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "interest"

    invoke-virtual {v2, v11}, Lcom/parse/ParseObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, " @"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "location"

    invoke-virtual {v2, v11}, Lcom/parse/ParseObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3}, Ljava/util/Date;->toLocaleString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v10, v6, v11}, Lcom/ntu/converge/ExploreFragment;->putData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_68
.end method

.method private putData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/HashMap;
    .registers 6
    .parameter "content"
    .parameter "creator"
    .parameter "time"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 72
    .local v0, item:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "content"

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    const-string v1, "creator"

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    const-string v1, "time"

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    return-object v0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 12
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    const/4 v7, 0x3

    const/4 v3, 0x0

    .line 22
    invoke-static {}, Lcom/ntu/converge/MainActivity;->getUserID()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/ntu/converge/ExploreFragment;->user_id:Ljava/lang/String;

    .line 23
    const v1, 0x7f030013

    invoke-virtual {p1, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 25
    .local v6, rootView:Landroid/view/View;
    invoke-direct {p0}, Lcom/ntu/converge/ExploreFragment;->buildData()Ljava/util/ArrayList;

    move-result-object v2

    .line 26
    .local v2, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    new-array v4, v7, [Ljava/lang/String;

    const-string v1, "content"

    aput-object v1, v4, v3

    const/4 v1, 0x1

    const-string v3, "creator"

    aput-object v3, v4, v1

    const/4 v1, 0x2

    const-string v3, "time"

    aput-object v3, v4, v1

    .line 27
    .local v4, from:[Ljava/lang/String;
    new-array v5, v7, [I

    fill-array-data v5, :array_38

    .line 28
    .local v5, to:[I
    new-instance v0, Landroid/widget/SimpleAdapter;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 29
    const v3, 0x7f030011

    .line 28
    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 30
    .local v0, adapter:Landroid/widget/SimpleAdapter;
    invoke-virtual {p0, v0}, Lcom/ntu/converge/ExploreFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 31
    return-object v6

    .line 27
    :array_38
    .array-data 0x4
        0x21t 0x0t 0x4t 0x7ft
        0x24t 0x0t 0x4t 0x7ft
        0x23t 0x0t 0x4t 0x7ft
    .end array-data
.end method
