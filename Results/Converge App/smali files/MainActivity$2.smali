.class Lcom/ntu/converge/MainActivity$2;
.super Lcom/parse/LogInCallback;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ntu/converge/MainActivity;->onLoginButtonClicked()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/ntu/converge/MainActivity;


# direct methods
.method constructor <init>(Lcom/ntu/converge/MainActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/ntu/converge/MainActivity$2;->this$0:Lcom/ntu/converge/MainActivity;

    .line 91
    invoke-direct {p0}, Lcom/parse/LogInCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public done(Lcom/parse/ParseUser;Lcom/parse/ParseException;)V
    .registers 5
    .parameter "user"
    .parameter "err"

    .prologue
    .line 94
    iget-object v0, p0, Lcom/ntu/converge/MainActivity$2;->this$0:Lcom/ntu/converge/MainActivity;

    #getter for: Lcom/ntu/converge/MainActivity;->progressDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/ntu/converge/MainActivity;->access$1(Lcom/ntu/converge/MainActivity;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 95
    if-nez p1, :cond_13

    .line 96
    const-string v0, "Converge"

    const-string v1, "Uh oh. The user cancelled the Facebook login."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    :goto_12
    return-void

    .line 97
    :cond_13
    invoke-virtual {p1}, Lcom/parse/ParseUser;->isNew()Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 98
    const-string v0, "Converge"

    const-string v1, "User signed up and logged in through Facebook!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    iget-object v0, p0, Lcom/ntu/converge/MainActivity$2;->this$0:Lcom/ntu/converge/MainActivity;

    #calls: Lcom/ntu/converge/MainActivity;->setUserDetails(Lcom/parse/ParseObject;Lcom/parse/ParseException;)V
    invoke-static {v0, p1, p2}, Lcom/ntu/converge/MainActivity;->access$2(Lcom/ntu/converge/MainActivity;Lcom/parse/ParseObject;Lcom/parse/ParseException;)V

    .line 100
    iget-object v0, p0, Lcom/ntu/converge/MainActivity$2;->this$0:Lcom/ntu/converge/MainActivity;

    #calls: Lcom/ntu/converge/MainActivity;->getUserId()V
    invoke-static {v0}, Lcom/ntu/converge/MainActivity;->access$3(Lcom/ntu/converge/MainActivity;)V

    goto :goto_12

    .line 102
    :cond_2b
    const-string v0, "Converge"

    const-string v1, "User logged in through Facebook!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    iget-object v0, p0, Lcom/ntu/converge/MainActivity$2;->this$0:Lcom/ntu/converge/MainActivity;

    #calls: Lcom/ntu/converge/MainActivity;->getUserId()V
    invoke-static {v0}, Lcom/ntu/converge/MainActivity;->access$3(Lcom/ntu/converge/MainActivity;)V

    goto :goto_12
.end method
