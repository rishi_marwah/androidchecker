.class Lcom/ntu/converge/MainActivity$4;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Lcom/facebook/Request$GraphUserCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ntu/converge/MainActivity;->setUserDetails(Lcom/parse/ParseObject;Lcom/parse/ParseException;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/ntu/converge/MainActivity;


# direct methods
.method constructor <init>(Lcom/ntu/converge/MainActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/ntu/converge/MainActivity$4;->this$0:Lcom/ntu/converge/MainActivity;

    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Lcom/facebook/model/GraphUser;Lcom/facebook/Response;)V
    .registers 8
    .parameter "user"
    .parameter "response"

    .prologue
    .line 164
    if-eqz p1, :cond_80

    .line 166
    new-instance v1, Lcom/parse/ParseObject;

    const-string v2, "User_data"

    invoke-direct {v1, v2}, Lcom/parse/ParseObject;-><init>(Ljava/lang/String;)V

    .line 171
    .local v1, userProfile:Lcom/parse/ParseObject;
    :try_start_9
    const-string v2, "facebookId"

    invoke-interface {p1}, Lcom/facebook/model/GraphUser;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/parse/ParseObject;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 172
    const-string v2, "name"

    invoke-interface {p1}, Lcom/facebook/model/GraphUser;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/parse/ParseObject;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 173
    invoke-interface {p1}, Lcom/facebook/model/GraphUser;->getLocation()Lcom/facebook/model/GraphLocation;

    move-result-object v2

    const-string v3, "name"

    invoke-interface {v2, v3}, Lcom/facebook/model/GraphLocation;->getProperty(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_38

    .line 174
    const-string v3, "location"

    .line 175
    invoke-interface {p1}, Lcom/facebook/model/GraphUser;->getLocation()Lcom/facebook/model/GraphLocation;

    move-result-object v2

    const-string v4, "name"

    invoke-interface {v2, v4}, Lcom/facebook/model/GraphLocation;->getProperty(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 174
    invoke-virtual {v1, v3, v2}, Lcom/parse/ParseObject;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 178
    :cond_38
    invoke-interface {p1}, Lcom/facebook/model/GraphUser;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/ntu/converge/MainActivity;->access$5(Ljava/lang/String;)V

    .line 180
    const-string v2, "gender"

    invoke-interface {p1, v2}, Lcom/facebook/model/GraphUser;->getProperty(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_54

    .line 181
    const-string v3, "gender"

    .line 182
    const-string v2, "gender"

    invoke-interface {p1, v2}, Lcom/facebook/model/GraphUser;->getProperty(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 181
    invoke-virtual {v1, v3, v2}, Lcom/parse/ParseObject;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 184
    :cond_54
    invoke-interface {p1}, Lcom/facebook/model/GraphUser;->getBirthday()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_63

    .line 185
    const-string v2, "birthday"

    .line 186
    invoke-interface {p1}, Lcom/facebook/model/GraphUser;->getBirthday()Ljava/lang/String;

    move-result-object v3

    .line 185
    invoke-virtual {v1, v2, v3}, Lcom/parse/ParseObject;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 189
    :cond_63
    invoke-static {}, Lcom/parse/ParseFacebookUtils;->getSession()Lcom/facebook/Session;

    move-result-object v2

    new-instance v3, Lcom/ntu/converge/MainActivity$4$1;

    invoke-direct {v3, p0, p1}, Lcom/ntu/converge/MainActivity$4$1;-><init>(Lcom/ntu/converge/MainActivity$4;Lcom/facebook/model/GraphUser;)V

    invoke-static {v2, v3}, Lcom/facebook/Request;->newMyFriendsRequest(Lcom/facebook/Session;Lcom/facebook/Request$GraphUserListCallback;)Lcom/facebook/Request;

    move-result-object v2

    .line 205
    invoke-virtual {v2}, Lcom/facebook/Request;->executeAsync()Lcom/facebook/RequestAsyncTask;

    .line 209
    invoke-virtual {v1}, Lcom/parse/ParseObject;->saveInBackground()V
    :try_end_76
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_76} :catch_77

    .line 221
    .end local v1           #userProfile:Lcom/parse/ParseObject;
    :goto_76
    return-void

    .line 214
    .restart local v1       #userProfile:Lcom/parse/ParseObject;
    :catch_77
    move-exception v0

    .line 215
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "Converge"

    .line 216
    const-string v3, "Error parsing returned user data."

    .line 215
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_76

    .line 218
    .end local v0           #e:Ljava/lang/Exception;
    .end local v1           #userProfile:Lcom/parse/ParseObject;
    :cond_80
    invoke-virtual {p2}, Lcom/facebook/Response;->getError()Lcom/facebook/FacebookRequestError;

    goto :goto_76
.end method
