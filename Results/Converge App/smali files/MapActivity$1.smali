.class Lcom/ntu/converge/MapActivity$1;
.super Ljava/lang/Object;
.source "MapActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ntu/converge/MapActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/ntu/converge/MapActivity;


# direct methods
.method constructor <init>(Lcom/ntu/converge/MapActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/ntu/converge/MapActivity$1;->this$0:Lcom/ntu/converge/MapActivity;

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 17
    .parameter "v"

    .prologue
    .line 46
    iget-object v1, p0, Lcom/ntu/converge/MapActivity$1;->this$0:Lcom/ntu/converge/MapActivity;

    #getter for: Lcom/ntu/converge/MapActivity;->dropdown:Landroid/widget/Spinner;
    invoke-static {v1}, Lcom/ntu/converge/MapActivity;->access$0(Lcom/ntu/converge/MapActivity;)Landroid/widget/Spinner;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 47
    .local v11, interest:Ljava/lang/String;
    iget-object v1, p0, Lcom/ntu/converge/MapActivity$1;->this$0:Lcom/ntu/converge/MapActivity;

    #getter for: Lcom/ntu/converge/MapActivity;->date:Landroid/widget/DatePicker;
    invoke-static {v1}, Lcom/ntu/converge/MapActivity;->access$1(Lcom/ntu/converge/MapActivity;)Landroid/widget/DatePicker;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v7

    .line 48
    .local v7, day:I
    iget-object v1, p0, Lcom/ntu/converge/MapActivity$1;->this$0:Lcom/ntu/converge/MapActivity;

    #getter for: Lcom/ntu/converge/MapActivity;->date:Landroid/widget/DatePicker;
    invoke-static {v1}, Lcom/ntu/converge/MapActivity;->access$1(Lcom/ntu/converge/MapActivity;)Landroid/widget/DatePicker;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/DatePicker;->getMonth()I

    move-result v1

    add-int/lit8 v13, v1, 0x1

    .line 49
    .local v13, month:I
    iget-object v1, p0, Lcom/ntu/converge/MapActivity$1;->this$0:Lcom/ntu/converge/MapActivity;

    #getter for: Lcom/ntu/converge/MapActivity;->date:Landroid/widget/DatePicker;
    invoke-static {v1}, Lcom/ntu/converge/MapActivity;->access$1(Lcom/ntu/converge/MapActivity;)Landroid/widget/DatePicker;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/DatePicker;->getYear()I

    move-result v14

    .line 50
    .local v14, year:I
    iget-object v1, p0, Lcom/ntu/converge/MapActivity$1;->this$0:Lcom/ntu/converge/MapActivity;

    #getter for: Lcom/ntu/converge/MapActivity;->time:Landroid/widget/TimePicker;
    invoke-static {v1}, Lcom/ntu/converge/MapActivity;->access$2(Lcom/ntu/converge/MapActivity;)Landroid/widget/TimePicker;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 51
    .local v9, hour:I
    iget-object v1, p0, Lcom/ntu/converge/MapActivity$1;->this$0:Lcom/ntu/converge/MapActivity;

    #getter for: Lcom/ntu/converge/MapActivity;->time:Landroid/widget/TimePicker;
    invoke-static {v1}, Lcom/ntu/converge/MapActivity;->access$2(Lcom/ntu/converge/MapActivity;)Landroid/widget/TimePicker;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 52
    .local v5, minute:I
    new-instance v0, Ljava/util/Date;

    add-int/lit16 v1, v14, -0x76c

    add-int/lit8 v2, v13, -0x1

    add-int/lit8 v3, v7, 0x1

    .line 53
    add-int/lit8 v4, v9, -0x1

    add-int/lit8 v4, v4, -0xf

    .line 52
    invoke-direct/range {v0 .. v5}, Ljava/util/Date;-><init>(IIIII)V

    .line 54
    .local v0, dt:Ljava/util/Date;
    iget-object v1, p0, Lcom/ntu/converge/MapActivity$1;->this$0:Lcom/ntu/converge/MapActivity;

    #getter for: Lcom/ntu/converge/MapActivity;->description:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/ntu/converge/MapActivity;->access$3(Lcom/ntu/converge/MapActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    .line 55
    .local v8, desc:Ljava/lang/String;
    iget-object v1, p0, Lcom/ntu/converge/MapActivity$1;->this$0:Lcom/ntu/converge/MapActivity;

    #getter for: Lcom/ntu/converge/MapActivity;->location:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/ntu/converge/MapActivity;->access$4(Lcom/ntu/converge/MapActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v12

    .line 56
    .local v12, loc:Ljava/lang/String;
    const-string v1, "All"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 57
    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 58
    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/ntu/converge/MapActivity$1;->this$0:Lcom/ntu/converge/MapActivity;

    #getter for: Lcom/ntu/converge/MapActivity;->gpsLocation:Lcom/parse/ParseGeoPoint;
    invoke-static {v3}, Lcom/ntu/converge/MapActivity;->access$5(Lcom/ntu/converge/MapActivity;)Lcom/parse/ParseGeoPoint;

    move-result-object v3

    invoke-virtual {v3}, Lcom/parse/ParseGeoPoint;->getLatitude()D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 59
    iget-object v3, p0, Lcom/ntu/converge/MapActivity$1;->this$0:Lcom/ntu/converge/MapActivity;

    #getter for: Lcom/ntu/converge/MapActivity;->gpsLocation:Lcom/parse/ParseGeoPoint;
    invoke-static {v3}, Lcom/ntu/converge/MapActivity;->access$5(Lcom/ntu/converge/MapActivity;)Lcom/parse/ParseGeoPoint;

    move-result-object v3

    invoke-virtual {v3}, Lcom/parse/ParseGeoPoint;->getLongitude()D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 56
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    new-instance v6, Lcom/parse/ParseObject;

    const-string v1, "Activities"

    invoke-direct {v6, v1}, Lcom/parse/ParseObject;-><init>(Ljava/lang/String;)V

    .line 61
    .local v6, activity:Lcom/parse/ParseObject;
    const-string v1, "user_id"

    iget-object v2, p0, Lcom/ntu/converge/MapActivity$1;->this$0:Lcom/ntu/converge/MapActivity;

    iget-object v2, v2, Lcom/ntu/converge/MapActivity;->userid:Ljava/lang/String;

    invoke-virtual {v6, v1, v2}, Lcom/parse/ParseObject;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 62
    const-string v1, "date"

    invoke-virtual {v6, v1, v0}, Lcom/parse/ParseObject;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 63
    const-string v1, "location"

    invoke-virtual {v6, v1, v12}, Lcom/parse/ParseObject;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 64
    const-string v1, "description"

    invoke-virtual {v6, v1, v8}, Lcom/parse/ParseObject;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 65
    const-string v1, "interest"

    invoke-virtual {v6, v1, v11}, Lcom/parse/ParseObject;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 66
    const-string v1, "gps"

    invoke-static {}, Lcom/ntu/converge/MainActivity;->getLocation()Lcom/parse/ParseGeoPoint;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Lcom/parse/ParseObject;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 67
    invoke-virtual {v6}, Lcom/parse/ParseObject;->saveInBackground()V

    .line 68
    new-instance v10, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ntu/converge/MapActivity$1;->this$0:Lcom/ntu/converge/MapActivity;

    const-class v2, Lcom/ntu/converge/HomeActivity;

    invoke-direct {v10, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 69
    .local v10, intent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/ntu/converge/MapActivity$1;->this$0:Lcom/ntu/converge/MapActivity;

    invoke-virtual {v1, v10}, Lcom/ntu/converge/MapActivity;->startActivity(Landroid/content/Intent;)V

    .line 70
    return-void
.end method
