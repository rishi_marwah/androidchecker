.class public Lcom/ntu/converge/ProfileFragment;
.super Landroid/support/v4/app/Fragment;
.source "ProfileFragment.java"


# instance fields
.field user_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 30
    invoke-static {}, Lcom/ntu/converge/MainActivity;->getUserID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ntu/converge/ProfileFragment;->user_id:Ljava/lang/String;

    .line 28
    return-void
.end method

.method private static OpenHttpConnection(Ljava/lang/String;)Ljava/io/InputStream;
    .registers 8
    .parameter "strURL"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    const/4 v3, 0x0

    .line 69
    .local v3, inputStream:Ljava/io/InputStream;
    new-instance v4, Ljava/net/URL;

    invoke-direct {v4, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 70
    .local v4, url:Ljava/net/URL;
    invoke-virtual {v4}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v1

    .line 73
    .local v1, conn:Ljava/net/URLConnection;
    :try_start_a
    move-object v0, v1

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v2, v0

    .line 74
    .local v2, httpConn:Ljava/net/HttpURLConnection;
    const-string v5, "GET"

    invoke-virtual {v2, v5}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 75
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->connect()V

    .line 77
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v5

    const/16 v6, 0xc8

    if-ne v5, v6, :cond_22

    .line 78
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_21
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_21} :catch_23

    move-result-object v3

    .line 82
    .end local v2           #httpConn:Ljava/net/HttpURLConnection;
    :cond_22
    :goto_22
    return-object v3

    .line 80
    :catch_23
    move-exception v5

    goto :goto_22
.end method

.method public static getFacebookProfilePicture(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .registers 8
    .parameter "userID"

    .prologue
    .line 34
    const/4 v0, 0x0

    .line 36
    .local v0, bm:Landroid/graphics/Bitmap;
    :try_start_1
    new-instance v3, Ljava/net/URL;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "http://graph.facebook.com/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/picture?style=small"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 38
    .local v3, image_value:Ljava/net/URL;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "http://graph.facebook.com/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/picture?width=800&height=600"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 40
    .local v4, url:Ljava/lang/String;
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 41
    .local v1, bmOptions:Landroid/graphics/BitmapFactory$Options;
    const/4 v5, 0x1

    iput v5, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 42
    invoke-static {v4, v1}, Lcom/ntu/converge/ProfileFragment;->loadBitmap(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_3b
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_3b} :catch_3d

    move-result-object v0

    .line 49
    .end local v1           #bmOptions:Landroid/graphics/BitmapFactory$Options;
    .end local v3           #image_value:Ljava/net/URL;
    .end local v4           #url:Ljava/lang/String;
    :goto_3c
    return-object v0

    .line 45
    :catch_3d
    move-exception v2

    .line 47
    .local v2, e:Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3c
.end method

.method public static loadBitmap(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .registers 5
    .parameter "URL"
    .parameter "options"

    .prologue
    .line 53
    const/4 v0, 0x0

    .line 54
    .local v0, bitmap:Landroid/graphics/Bitmap;
    const/4 v1, 0x0

    .line 57
    .local v1, in:Ljava/io/InputStream;
    :try_start_2
    invoke-static {p0}, Lcom/ntu/converge/ProfileFragment;->OpenHttpConnection(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 58
    const/4 v2, 0x0

    invoke-static {v1, v2, p1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 59
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_e} :catch_f

    .line 63
    :goto_e
    return-object v0

    .line 61
    :catch_f
    move-exception v2

    goto :goto_e
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 33
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 91
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ntu/converge/ProfileFragment;->user_id:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/ntu/converge/ProfileFragment;->getFacebookProfilePicture(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 93
    .local v4, bitmap:Landroid/graphics/Bitmap;
    const v25, 0x7f030014

    .line 94
    const/16 v26, 0x0

    .line 93
    move-object/from16 v0, p1

    move/from16 v1, v25

    move-object/from16 v2, p2

    move/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v19

    .line 96
    .local v19, rootView:Landroid/view/View;
    const-string v25, "UserInterests"

    invoke-static/range {v25 .. v25}, Lcom/parse/ParseQuery;->getQuery(Ljava/lang/String;)Lcom/parse/ParseQuery;

    move-result-object v15

    .line 97
    .local v15, interestsQuery:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    const-string v25, "user_id"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ntu/converge/ProfileFragment;->user_id:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v15, v0, v1}, Lcom/parse/ParseQuery;->whereEqualTo(Ljava/lang/String;Ljava/lang/Object;)Lcom/parse/ParseQuery;

    .line 98
    const/4 v13, 0x0

    .line 99
    .local v13, interests:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 101
    .local v12, interest_list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_36
    invoke-virtual {v15}, Lcom/parse/ParseQuery;->find()Ljava/util/List;
    :try_end_39
    .catch Lcom/parse/ParseException; {:try_start_36 .. :try_end_39} :catch_147

    move-result-object v13

    .line 107
    :goto_3a
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 108
    .local v23, user_interests_ids:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v25

    :goto_43
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-nez v26, :cond_14d

    .line 111
    const-string v25, "Interests"

    invoke-static/range {v25 .. v25}, Lcom/parse/ParseQuery;->getQuery(Ljava/lang/String;)Lcom/parse/ParseQuery;

    move-result-object v16

    .line 112
    .local v16, interestsQuery2:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    const-string v25, "interest_id"

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/parse/ParseQuery;->whereContainedIn(Ljava/lang/String;Ljava/util/Collection;)Lcom/parse/ParseQuery;

    .line 113
    const/4 v14, 0x0

    .line 115
    .local v14, interestsL:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    :try_start_5b
    invoke-virtual/range {v16 .. v16}, Lcom/parse/ParseQuery;->find()Ljava/util/List;
    :try_end_5e
    .catch Lcom/parse/ParseException; {:try_start_5b .. :try_end_5e} :catch_168

    move-result-object v14

    .line 121
    :goto_5f
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    .line 122
    .local v22, user_interests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v25

    :goto_68
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-nez v26, :cond_16e

    .line 126
    const-string v25, "User_data"

    invoke-static/range {v25 .. v25}, Lcom/parse/ParseQuery;->getQuery(Ljava/lang/String;)Lcom/parse/ParseQuery;

    move-result-object v21

    .line 127
    .local v21, userQuery:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    const-string v25, "facebookId"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ntu/converge/ProfileFragment;->user_id:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lcom/parse/ParseQuery;->whereEqualTo(Ljava/lang/String;Ljava/lang/Object;)Lcom/parse/ParseQuery;

    .line 128
    const/16 v24, 0x0

    .line 130
    .local v24, users:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    :try_start_87
    invoke-virtual/range {v21 .. v21}, Lcom/parse/ParseQuery;->find()Ljava/util/List;

    move-result-object v24

    .line 131
    const-string v25, "User check"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v27

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a1
    .catch Lcom/parse/ParseException; {:try_start_87 .. :try_end_a1} :catch_19f

    .line 136
    :goto_a1
    const/16 v25, 0x0

    invoke-interface/range {v24 .. v25}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/parse/ParseObject;

    const-string v26, "location"

    invoke-virtual/range {v25 .. v26}, Lcom/parse/ParseObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 137
    .local v17, location:Ljava/lang/String;
    const/16 v25, 0x0

    invoke-interface/range {v24 .. v25}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/parse/ParseObject;

    const-string v26, "name"

    invoke-virtual/range {v25 .. v26}, Lcom/parse/ParseObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 138
    .local v18, name:Ljava/lang/String;
    const/16 v25, 0x0

    invoke-interface/range {v24 .. v25}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/parse/ParseObject;

    const-string v26, "birthday"

    invoke-virtual/range {v25 .. v26}, Lcom/parse/ParseObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 141
    .local v6, dob:Ljava/lang/String;
    const v25, 0x7f040025

    move-object/from16 v0, v19

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    .line 142
    .local v20, t:Landroid/widget/TextView;
    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    const v25, 0x7f04002a

    move-object/from16 v0, v19

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    .end local v20           #t:Landroid/widget/TextView;
    check-cast v20, Landroid/widget/TextView;

    .line 148
    .restart local v20       #t:Landroid/widget/TextView;
    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    const v25, 0x7f040028

    move-object/from16 v0, v19

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    .end local v20           #t:Landroid/widget/TextView;
    check-cast v20, Landroid/widget/TextView;

    .line 151
    .restart local v20       #t:Landroid/widget/TextView;
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v25

    const/16 v26, 0x1

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->length()I

    move-result v27

    add-int/lit8 v27, v27, -0x1

    invoke-virtual/range {v25 .. v27}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    const v25, 0x7f040026

    move-object/from16 v0, v19

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    .line 154
    .local v10, im:Landroid/widget/ImageView;
    invoke-virtual {v10, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 157
    const v25, 0x7f040029

    move-object/from16 v0, v19

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    .line 158
    .local v5, button:Landroid/widget/Button;
    new-instance v25, Lcom/ntu/converge/ProfileFragment$1;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/ntu/converge/ProfileFragment$1;-><init>(Lcom/ntu/converge/ProfileFragment;)V

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    return-object v19

    .line 102
    .end local v5           #button:Landroid/widget/Button;
    .end local v6           #dob:Ljava/lang/String;
    .end local v10           #im:Landroid/widget/ImageView;
    .end local v14           #interestsL:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    .end local v16           #interestsQuery2:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    .end local v17           #location:Ljava/lang/String;
    .end local v18           #name:Ljava/lang/String;
    .end local v20           #t:Landroid/widget/TextView;
    .end local v21           #userQuery:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    .end local v22           #user_interests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v23           #user_interests_ids:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v24           #users:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    :catch_147
    move-exception v8

    .line 104
    .local v8, e3:Lcom/parse/ParseException;
    invoke-virtual {v8}, Lcom/parse/ParseException;->printStackTrace()V

    goto/16 :goto_3a

    .line 108
    .end local v8           #e3:Lcom/parse/ParseException;
    .restart local v23       #user_interests_ids:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_14d
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/parse/ParseObject;

    .line 109
    .local v9, id:Lcom/parse/ParseObject;
    const-string v26, "interest_id"

    move-object/from16 v0, v26

    invoke-virtual {v9, v0}, Lcom/parse/ParseObject;->getInt(Ljava/lang/String;)I

    move-result v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_43

    .line 116
    .end local v9           #id:Lcom/parse/ParseObject;
    .restart local v14       #interestsL:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    .restart local v16       #interestsQuery2:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    :catch_168
    move-exception v7

    .line 118
    .local v7, e:Lcom/parse/ParseException;
    invoke-virtual {v7}, Lcom/parse/ParseException;->printStackTrace()V

    goto/16 :goto_5f

    .line 122
    .end local v7           #e:Lcom/parse/ParseException;
    .restart local v22       #user_interests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_16e
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/parse/ParseObject;

    .line 123
    .local v11, interest:Lcom/parse/ParseObject;
    const-string v26, "name"

    move-object/from16 v0, v26

    invoke-virtual {v11, v0}, Lcom/parse/ParseObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    const-string v26, "usr int check"

    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "name"

    move-object/from16 v0, v28

    invoke-virtual {v11, v0}, Lcom/parse/ParseObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v28 .. v28}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_68

    .line 132
    .end local v11           #interest:Lcom/parse/ParseObject;
    .restart local v21       #userQuery:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    .restart local v24       #users:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    :catch_19f
    move-exception v7

    .line 134
    .restart local v7       #e:Lcom/parse/ParseException;
    invoke-virtual {v7}, Lcom/parse/ParseException;->printStackTrace()V

    goto/16 :goto_a1
.end method
