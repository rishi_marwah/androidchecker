.class public Lcom/ntu/converge/MapActivity;
.super Landroid/app/Activity;
.source "MapActivity.java"


# instance fields
.field private date:Landroid/widget/DatePicker;

.field private description:Landroid/widget/EditText;

.field private dropdown:Landroid/widget/Spinner;

.field private gpsLocation:Lcom/parse/ParseGeoPoint;

.field private location:Landroid/widget/EditText;

.field private post:Landroid/widget/Button;

.field private time:Landroid/widget/TimePicker;

.field userid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/ntu/converge/MapActivity;)Landroid/widget/Spinner;
    .registers 2
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcom/ntu/converge/MapActivity;->dropdown:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$1(Lcom/ntu/converge/MapActivity;)Landroid/widget/DatePicker;
    .registers 2
    .parameter

    .prologue
    .line 28
    iget-object v0, p0, Lcom/ntu/converge/MapActivity;->date:Landroid/widget/DatePicker;

    return-object v0
.end method

.method static synthetic access$2(Lcom/ntu/converge/MapActivity;)Landroid/widget/TimePicker;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/ntu/converge/MapActivity;->time:Landroid/widget/TimePicker;

    return-object v0
.end method

.method static synthetic access$3(Lcom/ntu/converge/MapActivity;)Landroid/widget/EditText;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcom/ntu/converge/MapActivity;->description:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$4(Lcom/ntu/converge/MapActivity;)Landroid/widget/EditText;
    .registers 2
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, Lcom/ntu/converge/MapActivity;->location:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$5(Lcom/ntu/converge/MapActivity;)Lcom/parse/ParseGeoPoint;
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lcom/ntu/converge/MapActivity;->gpsLocation:Lcom/parse/ParseGeoPoint;

    return-object v0
.end method


# virtual methods
.method public addItemsOnSpinner()V
    .registers 9

    .prologue
    .line 74
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 76
    .local v4, interests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v6, "Interests"

    invoke-static {v6}, Lcom/parse/ParseQuery;->getQuery(Ljava/lang/String;)Lcom/parse/ParseQuery;

    move-result-object v5

    .line 77
    .local v5, queryUserInterest:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    const/4 v0, 0x0

    .line 79
    .local v0, allInterests:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    :try_start_c
    invoke-virtual {v5}, Lcom/parse/ParseQuery;->find()Ljava/util/List;

    move-result-object v0

    .line 80
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_14
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z
    :try_end_17
    .catch Lcom/parse/ParseException; {:try_start_c .. :try_end_17} :catch_3e

    move-result v7

    if-nez v7, :cond_2e

    .line 87
    :goto_1a
    new-instance v1, Landroid/widget/ArrayAdapter;

    .line 88
    const v6, 0x1090008

    .line 87
    invoke-direct {v1, p0, v6, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 90
    .local v1, dataAdapter:Landroid/widget/ArrayAdapter;,"Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    const v6, 0x1090009

    invoke-virtual {v1, v6}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 91
    iget-object v6, p0, Lcom/ntu/converge/MapActivity;->dropdown:Landroid/widget/Spinner;

    invoke-virtual {v6, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 92
    return-void

    .line 80
    .end local v1           #dataAdapter:Landroid/widget/ArrayAdapter;,"Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    :cond_2e
    :try_start_2e
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/parse/ParseObject;

    .line 81
    .local v3, interest:Lcom/parse/ParseObject;
    const-string v7, "name"

    invoke-virtual {v3, v7}, Lcom/parse/ParseObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3d
    .catch Lcom/parse/ParseException; {:try_start_2e .. :try_end_3d} :catch_3e

    goto :goto_14

    .line 83
    .end local v3           #interest:Lcom/parse/ParseObject;
    :catch_3e
    move-exception v2

    .line 85
    .local v2, e1:Lcom/parse/ParseException;
    invoke-virtual {v2}, Lcom/parse/ParseException;->printStackTrace()V

    goto :goto_1a
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter "savedInstanceState"

    .prologue
    .line 33
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 34
    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Lcom/ntu/converge/MapActivity;->setContentView(I)V

    .line 35
    const v0, 0x7f040005

    invoke-virtual {p0, v0}, Lcom/ntu/converge/MapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/ntu/converge/MapActivity;->description:Landroid/widget/EditText;

    .line 36
    const v0, 0x7f040006

    invoke-virtual {p0, v0}, Lcom/ntu/converge/MapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/ntu/converge/MapActivity;->location:Landroid/widget/EditText;

    .line 37
    const v0, 0x7f040008

    invoke-virtual {p0, v0}, Lcom/ntu/converge/MapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/DatePicker;

    iput-object v0, p0, Lcom/ntu/converge/MapActivity;->date:Landroid/widget/DatePicker;

    .line 38
    const v0, 0x7f040007

    invoke-virtual {p0, v0}, Lcom/ntu/converge/MapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TimePicker;

    iput-object v0, p0, Lcom/ntu/converge/MapActivity;->time:Landroid/widget/TimePicker;

    .line 39
    const v0, 0x7f040009

    invoke-virtual {p0, v0}, Lcom/ntu/converge/MapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/ntu/converge/MapActivity;->post:Landroid/widget/Button;

    .line 40
    const v0, 0x7f040004

    invoke-virtual {p0, v0}, Lcom/ntu/converge/MapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/ntu/converge/MapActivity;->dropdown:Landroid/widget/Spinner;

    .line 41
    invoke-virtual {p0}, Lcom/ntu/converge/MapActivity;->addItemsOnSpinner()V

    .line 42
    invoke-static {}, Lcom/ntu/converge/MainActivity;->getUserID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ntu/converge/MapActivity;->userid:Ljava/lang/String;

    .line 43
    iget-object v0, p0, Lcom/ntu/converge/MapActivity;->post:Landroid/widget/Button;

    new-instance v1, Lcom/ntu/converge/MapActivity$1;

    invoke-direct {v1, p0}, Lcom/ntu/converge/MapActivity$1;-><init>(Lcom/ntu/converge/MapActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    return-void
.end method
