.class public Lcom/ntu/converge/FeedFragment;
.super Landroid/support/v4/app/ListFragment;
.source "FeedFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ntu/converge/FeedFragment$GetDataTask;
    }
.end annotation


# instance fields
.field user_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    return-void
.end method

.method private buildData()Ljava/util/ArrayList;
    .registers 36
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 51
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 52
    .local v25, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    const-string v31, "Friends"

    invoke-static/range {v31 .. v31}, Lcom/parse/ParseQuery;->getQuery(Ljava/lang/String;)Lcom/parse/ParseQuery;

    move-result-object v15

    .line 53
    .local v15, friendsQuery:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    const-string v31, "user_id"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ntu/converge/FeedFragment;->user_id:Ljava/lang/String;

    move-object/from16 v32, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v15, v0, v1}, Lcom/parse/ParseQuery;->whereEqualTo(Ljava/lang/String;Ljava/lang/Object;)Lcom/parse/ParseQuery;

    .line 54
    const/4 v13, 0x0

    .line 55
    .local v13, friends:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 57
    .local v17, friends_ids:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_20
    invoke-virtual {v15}, Lcom/parse/ParseQuery;->find()Ljava/util/List;
    :try_end_23
    .catch Lcom/parse/ParseException; {:try_start_20 .. :try_end_23} :catch_131

    move-result-object v13

    .line 62
    :goto_24
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v31

    :goto_28
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    move-result v32

    if-nez v32, :cond_137

    .line 65
    const-string v31, "User_data"

    invoke-static/range {v31 .. v31}, Lcom/parse/ParseQuery;->getQuery(Ljava/lang/String;)Lcom/parse/ParseQuery;

    move-result-object v16

    .line 66
    .local v16, friends_fbQuery:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    const-string v31, "facebookId"

    move-object/from16 v0, v16

    move-object/from16 v1, v31

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/parse/ParseQuery;->whereContainedIn(Ljava/lang/String;Ljava/util/Collection;)Lcom/parse/ParseQuery;

    .line 67
    const/4 v14, 0x0

    .line 69
    .local v14, friendsFb:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    :try_start_40
    invoke-virtual/range {v16 .. v16}, Lcom/parse/ParseQuery;->find()Ljava/util/List;
    :try_end_43
    .catch Lcom/parse/ParseException; {:try_start_40 .. :try_end_43} :catch_14e

    move-result-object v14

    .line 74
    :goto_44
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 75
    .local v18, friends_ids_in_converge:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v31

    :goto_4d
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    move-result v32

    if-nez v32, :cond_154

    .line 78
    const-string v31, "UserInterests"

    invoke-static/range {v31 .. v31}, Lcom/parse/ParseQuery;->getQuery(Ljava/lang/String;)Lcom/parse/ParseQuery;

    move-result-object v22

    .line 79
    .local v22, interestIdsQuery:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    const-string v31, "user_id"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ntu/converge/FeedFragment;->user_id:Ljava/lang/String;

    move-object/from16 v32, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lcom/parse/ParseQuery;->whereEqualTo(Ljava/lang/String;Ljava/lang/Object;)Lcom/parse/ParseQuery;

    .line 80
    const/16 v21, 0x0

    .line 82
    .local v21, interestIds:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    :try_start_6c
    invoke-virtual/range {v22 .. v22}, Lcom/parse/ParseQuery;->find()Ljava/util/List;
    :try_end_6f
    .catch Lcom/parse/ParseException; {:try_start_6c .. :try_end_6f} :catch_16b

    move-result-object v21

    .line 87
    :goto_70
    new-instance v29, Ljava/util/ArrayList;

    invoke-direct/range {v29 .. v29}, Ljava/util/ArrayList;-><init>()V

    .line 88
    .local v29, user_interests_ids:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v31

    :goto_79
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    move-result v32

    if-nez v32, :cond_171

    .line 91
    const-string v31, "Interests"

    invoke-static/range {v31 .. v31}, Lcom/parse/ParseQuery;->getQuery(Ljava/lang/String;)Lcom/parse/ParseQuery;

    move-result-object v24

    .line 92
    .local v24, interestsQuery:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    const-string v31, "interest_id"

    move-object/from16 v0, v24

    move-object/from16 v1, v31

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/parse/ParseQuery;->whereContainedIn(Ljava/lang/String;Ljava/util/Collection;)Lcom/parse/ParseQuery;

    .line 93
    const/16 v23, 0x0

    .line 95
    .local v23, interests:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    :try_start_92
    invoke-virtual/range {v24 .. v24}, Lcom/parse/ParseQuery;->find()Ljava/util/List;
    :try_end_95
    .catch Lcom/parse/ParseException; {:try_start_92 .. :try_end_95} :catch_18e

    move-result-object v23

    .line 101
    :goto_96
    new-instance v28, Ljava/util/ArrayList;

    invoke-direct/range {v28 .. v28}, Ljava/util/ArrayList;-><init>()V

    .line 102
    .local v28, user_interests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface/range {v23 .. v23}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v31

    :goto_9f
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    move-result v32

    if-nez v32, :cond_194

    .line 106
    const-string v31, "Activities"

    invoke-static/range {v31 .. v31}, Lcom/parse/ParseQuery;->getQuery(Ljava/lang/String;)Lcom/parse/ParseQuery;

    move-result-object v5

    .line 107
    .local v5, activitiesQuery:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    const-string v31, "user_id"

    move-object/from16 v0, v31

    move-object/from16 v1, v18

    invoke-virtual {v5, v0, v1}, Lcom/parse/ParseQuery;->whereContainedIn(Ljava/lang/String;Ljava/util/Collection;)Lcom/parse/ParseQuery;

    .line 108
    const-string v31, "interest"

    move-object/from16 v0, v31

    move-object/from16 v1, v28

    invoke-virtual {v5, v0, v1}, Lcom/parse/ParseQuery;->whereContainedIn(Ljava/lang/String;Ljava/util/Collection;)Lcom/parse/ParseQuery;

    .line 109
    const-string v31, "gps"

    invoke-static {}, Lcom/ntu/converge/MainActivity;->getLocation()Lcom/parse/ParseGeoPoint;

    move-result-object v32

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v5, v0, v1}, Lcom/parse/ParseQuery;->whereNear(Ljava/lang/String;Lcom/parse/ParseGeoPoint;)Lcom/parse/ParseQuery;

    .line 111
    const/4 v4, 0x0

    .line 113
    .local v4, activities:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    :try_start_cb
    invoke-virtual {v5}, Lcom/parse/ParseQuery;->find()Ljava/util/List;

    move-result-object v4

    .line 114
    const-string v31, "act check"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v33

    invoke-static/range {v33 .. v33}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e5
    .catch Lcom/parse/ParseException; {:try_start_cb .. :try_end_e5} :catch_1c9

    .line 120
    :goto_e5
    const-string v31, "User_data"

    invoke-static/range {v31 .. v31}, Lcom/parse/ParseQuery;->getQuery(Ljava/lang/String;)Lcom/parse/ParseQuery;

    move-result-object v27

    .line 121
    .local v27, userQuery:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    const-string v31, "facebookId"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ntu/converge/FeedFragment;->user_id:Ljava/lang/String;

    move-object/from16 v32, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lcom/parse/ParseQuery;->whereEqualTo(Ljava/lang/String;Ljava/lang/Object;)Lcom/parse/ParseQuery;

    .line 122
    const/16 v30, 0x0

    .line 124
    .local v30, users:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    :try_start_fe
    invoke-virtual/range {v27 .. v27}, Lcom/parse/ParseQuery;->find()Ljava/util/List;

    move-result-object v30

    .line 125
    const-string v31, "User check"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-interface/range {v30 .. v30}, Ljava/util/List;->size()I

    move-result v33

    invoke-static/range {v33 .. v33}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_118
    .catch Lcom/parse/ParseException; {:try_start_fe .. :try_end_118} :catch_1cf

    .line 132
    :goto_118
    const/16 v31, 0x0

    invoke-interface/range {v30 .. v31}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/parse/ParseObject;

    const-string v32, "name"

    invoke-virtual/range {v31 .. v32}, Lcom/parse/ParseObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 134
    .local v26, name:Ljava/lang/String;
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v31

    :goto_12a
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    move-result v32

    if-nez v32, :cond_1d5

    .line 139
    return-object v25

    .line 58
    .end local v4           #activities:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    .end local v5           #activitiesQuery:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    .end local v14           #friendsFb:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    .end local v16           #friends_fbQuery:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    .end local v18           #friends_ids_in_converge:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v21           #interestIds:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    .end local v22           #interestIdsQuery:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    .end local v23           #interests:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    .end local v24           #interestsQuery:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    .end local v26           #name:Ljava/lang/String;
    .end local v27           #userQuery:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    .end local v28           #user_interests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v29           #user_interests_ids:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v30           #users:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    :catch_131
    move-exception v11

    .line 60
    .local v11, e3:Lcom/parse/ParseException;
    invoke-virtual {v11}, Lcom/parse/ParseException;->printStackTrace()V

    goto/16 :goto_24

    .line 62
    .end local v11           #e3:Lcom/parse/ParseException;
    :cond_137
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/parse/ParseObject;

    .line 63
    .local v12, friend:Lcom/parse/ParseObject;
    const-string v32, "friend_id"

    move-object/from16 v0, v32

    invoke-virtual {v12, v0}, Lcom/parse/ParseObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v17

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_28

    .line 70
    .end local v12           #friend:Lcom/parse/ParseObject;
    .restart local v14       #friendsFb:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    .restart local v16       #friends_fbQuery:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    :catch_14e
    move-exception v10

    .line 72
    .local v10, e2:Lcom/parse/ParseException;
    invoke-virtual {v10}, Lcom/parse/ParseException;->printStackTrace()V

    goto/16 :goto_44

    .line 75
    .end local v10           #e2:Lcom/parse/ParseException;
    .restart local v18       #friends_ids_in_converge:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_154
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/parse/ParseObject;

    .line 76
    .restart local v12       #friend:Lcom/parse/ParseObject;
    const-string v32, "facebookId"

    move-object/from16 v0, v32

    invoke-virtual {v12, v0}, Lcom/parse/ParseObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v18

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4d

    .line 83
    .end local v12           #friend:Lcom/parse/ParseObject;
    .restart local v21       #interestIds:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    .restart local v22       #interestIdsQuery:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    :catch_16b
    move-exception v9

    .line 85
    .local v9, e1:Lcom/parse/ParseException;
    invoke-virtual {v9}, Lcom/parse/ParseException;->printStackTrace()V

    goto/16 :goto_70

    .line 88
    .end local v9           #e1:Lcom/parse/ParseException;
    .restart local v29       #user_interests_ids:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_171
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/parse/ParseObject;

    .line 89
    .local v19, id:Lcom/parse/ParseObject;
    const-string v32, "interest_id"

    move-object/from16 v0, v19

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/parse/ParseObject;->getInt(Ljava/lang/String;)I

    move-result v32

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, v29

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_79

    .line 96
    .end local v19           #id:Lcom/parse/ParseObject;
    .restart local v23       #interests:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    .restart local v24       #interestsQuery:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    :catch_18e
    move-exception v8

    .line 98
    .local v8, e:Lcom/parse/ParseException;
    invoke-virtual {v8}, Lcom/parse/ParseException;->printStackTrace()V

    goto/16 :goto_96

    .line 102
    .end local v8           #e:Lcom/parse/ParseException;
    .restart local v28       #user_interests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_194
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/parse/ParseObject;

    .line 103
    .local v20, interest:Lcom/parse/ParseObject;
    const-string v32, "name"

    move-object/from16 v0, v20

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/parse/ParseObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v28

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    const-string v32, "usr int check"

    new-instance v33, Ljava/lang/StringBuilder;

    const-string v34, "name"

    move-object/from16 v0, v20

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/parse/ParseObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    invoke-static/range {v34 .. v34}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v34

    invoke-direct/range {v33 .. v34}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_9f

    .line 115
    .end local v20           #interest:Lcom/parse/ParseObject;
    .restart local v4       #activities:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    .restart local v5       #activitiesQuery:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    :catch_1c9
    move-exception v8

    .line 117
    .restart local v8       #e:Lcom/parse/ParseException;
    invoke-virtual {v8}, Lcom/parse/ParseException;->printStackTrace()V

    goto/16 :goto_e5

    .line 126
    .end local v8           #e:Lcom/parse/ParseException;
    .restart local v27       #userQuery:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    .restart local v30       #users:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    :catch_1cf
    move-exception v8

    .line 128
    .restart local v8       #e:Lcom/parse/ParseException;
    invoke-virtual {v8}, Lcom/parse/ParseException;->printStackTrace()V

    goto/16 :goto_118

    .line 134
    .end local v8           #e:Lcom/parse/ParseException;
    .restart local v26       #name:Ljava/lang/String;
    :cond_1d5
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/parse/ParseObject;

    .line 135
    .local v6, activity:Lcom/parse/ParseObject;
    const-string v32, "date"

    move-object/from16 v0, v32

    invoke-virtual {v6, v0}, Lcom/parse/ParseObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Date;

    .line 136
    .local v7, date:Ljava/util/Date;
    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "interest"

    move-object/from16 v0, v33

    invoke-virtual {v6, v0}, Lcom/parse/ParseObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v33, " @"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "location"

    move-object/from16 v0, v33

    invoke-virtual {v6, v0}, Lcom/parse/ParseObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-virtual {v7}, Ljava/util/Date;->toLocaleString()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    move-object/from16 v2, v26

    move-object/from16 v3, v33

    invoke-direct {v0, v1, v2, v3}, Lcom/ntu/converge/FeedFragment;->putData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v32

    move-object/from16 v0, v25

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_12a
.end method

.method private putData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/HashMap;
    .registers 6
    .parameter "content"
    .parameter "creator"
    .parameter "time"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 144
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 145
    .local v0, item:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "content"

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    const-string v1, "creator"

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    const-string v1, "time"

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    return-object v0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 13
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x0

    .line 27
    invoke-static {}, Lcom/ntu/converge/MainActivity;->getUserID()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/ntu/converge/FeedFragment;->user_id:Ljava/lang/String;

    .line 28
    const v1, 0x7f030013

    invoke-virtual {p1, v1, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 31
    .local v6, rootView:Landroid/view/View;
    invoke-direct {p0}, Lcom/ntu/converge/FeedFragment;->buildData()Ljava/util/ArrayList;

    move-result-object v2

    .line 32
    .local v2, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    new-array v4, v8, [Ljava/lang/String;

    const-string v1, "content"

    aput-object v1, v4, v7

    const/4 v1, 0x1

    const-string v3, "creator"

    aput-object v3, v4, v1

    const/4 v1, 0x2

    const-string v3, "time"

    aput-object v3, v4, v1

    .line 33
    .local v4, from:[Ljava/lang/String;
    new-array v5, v8, [I

    fill-array-data v5, :array_4a

    .line 34
    .local v5, to:[I
    new-instance v0, Landroid/widget/SimpleAdapter;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 35
    const v3, 0x7f030011

    .line 34
    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 36
    .local v0, adapter:Landroid/widget/SimpleAdapter;
    invoke-virtual {p0, v0}, Lcom/ntu/converge/FeedFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    move-object v1, v6

    .line 38
    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/markupartist/android/widget/PullToRefreshListView;

    new-instance v3, Lcom/ntu/converge/FeedFragment$1;

    invoke-direct {v3, p0}, Lcom/ntu/converge/FeedFragment$1;-><init>(Lcom/ntu/converge/FeedFragment;)V

    invoke-virtual {v1, v3}, Lcom/markupartist/android/widget/PullToRefreshListView;->setOnRefreshListener(Lcom/markupartist/android/widget/PullToRefreshListView$OnRefreshListener;)V

    .line 47
    return-object v6

    .line 33
    nop

    :array_4a
    .array-data 0x4
        0x21t 0x0t 0x4t 0x7ft
        0x24t 0x0t 0x4t 0x7ft
        0x23t 0x0t 0x4t 0x7ft
    .end array-data
.end method
