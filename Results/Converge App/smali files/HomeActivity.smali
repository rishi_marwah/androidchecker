.class public Lcom/ntu/converge/HomeActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "HomeActivity.java"

# interfaces
.implements Landroid/app/ActionBar$TabListener;


# instance fields
.field private actionBar:Landroid/app/ActionBar;

.field private mAdapter:Lcom/ntu/converge/TabsPagerAdapter;

.field private tabs:[Ljava/lang/String;

.field private viewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>()V
    .registers 4

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 25
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "Feed"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Explore"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "Profile"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/ntu/converge/HomeActivity;->tabs:[Ljava/lang/String;

    .line 19
    return-void
.end method

.method static synthetic access$0(Lcom/ntu/converge/HomeActivity;)Landroid/app/ActionBar;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcom/ntu/converge/HomeActivity;->actionBar:Landroid/app/ActionBar;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .registers 8
    .parameter "savedInstanceState"

    .prologue
    const/4 v2, 0x0

    .line 29
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 30
    const/high16 v1, 0x7f03

    invoke-virtual {p0, v1}, Lcom/ntu/converge/HomeActivity;->setContentView(I)V

    .line 33
    const v1, 0x7f040003

    invoke-virtual {p0, v1}, Lcom/ntu/converge/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ViewPager;

    iput-object v1, p0, Lcom/ntu/converge/HomeActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    .line 34
    invoke-virtual {p0}, Lcom/ntu/converge/HomeActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    iput-object v1, p0, Lcom/ntu/converge/HomeActivity;->actionBar:Landroid/app/ActionBar;

    .line 35
    new-instance v1, Lcom/ntu/converge/TabsPagerAdapter;

    invoke-virtual {p0}, Lcom/ntu/converge/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/ntu/converge/TabsPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    iput-object v1, p0, Lcom/ntu/converge/HomeActivity;->mAdapter:Lcom/ntu/converge/TabsPagerAdapter;

    .line 37
    iget-object v1, p0, Lcom/ntu/converge/HomeActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/ntu/converge/HomeActivity;->mAdapter:Lcom/ntu/converge/TabsPagerAdapter;

    invoke-virtual {v1, v3}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 38
    iget-object v1, p0, Lcom/ntu/converge/HomeActivity;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 39
    iget-object v1, p0, Lcom/ntu/converge/HomeActivity;->actionBar:Landroid/app/ActionBar;

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 42
    iget-object v3, p0, Lcom/ntu/converge/HomeActivity;->tabs:[Ljava/lang/String;

    array-length v4, v3

    move v1, v2

    :goto_3b
    if-lt v1, v4, :cond_48

    .line 50
    iget-object v1, p0, Lcom/ntu/converge/HomeActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    new-instance v2, Lcom/ntu/converge/HomeActivity$1;

    invoke-direct {v2, p0}, Lcom/ntu/converge/HomeActivity$1;-><init>(Lcom/ntu/converge/HomeActivity;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 68
    return-void

    .line 42
    :cond_48
    aget-object v0, v3, v1

    .line 43
    .local v0, tab_name:Ljava/lang/String;
    iget-object v2, p0, Lcom/ntu/converge/HomeActivity;->actionBar:Landroid/app/ActionBar;

    iget-object v5, p0, Lcom/ntu/converge/HomeActivity;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v5}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    move-result-object v5

    .line 44
    invoke-virtual {v5, p0}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    move-result-object v5

    .line 43
    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;)V

    .line 42
    add-int/lit8 v1, v1, 0x1

    goto :goto_3b
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/ntu/converge/HomeActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f09

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 78
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 7
    .parameter "item"

    .prologue
    const/4 v3, 0x1

    .line 83
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    packed-switch v4, :pswitch_data_36

    .line 99
    :pswitch_8
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    :goto_c
    return v3

    .line 85
    :pswitch_d
    new-instance v0, Landroid/content/Intent;

    const-class v4, Lcom/ntu/converge/MapActivity;

    invoke-direct {v0, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 86
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/ntu/converge/HomeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_c

    .line 89
    .end local v0           #intent:Landroid/content/Intent;
    :pswitch_18
    invoke-static {}, Lcom/parse/ParseFacebookUtils;->getSession()Lcom/facebook/Session;

    move-result-object v2

    .line 90
    .local v2, session:Lcom/facebook/Session;
    if-eqz v2, :cond_27

    invoke-virtual {v2}, Lcom/facebook/Session;->isOpened()Z

    move-result v4

    if-eqz v4, :cond_27

    .line 91
    invoke-virtual {v2}, Lcom/facebook/Session;->closeAndClearTokenInformation()V

    .line 94
    :cond_27
    invoke-static {}, Lcom/parse/ParseUser;->logOut()V

    .line 95
    new-instance v1, Landroid/content/Intent;

    const-class v4, Lcom/ntu/converge/MainActivity;

    invoke-direct {v1, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 96
    .local v1, intent2:Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/ntu/converge/HomeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_c

    .line 83
    nop

    :pswitch_data_36
    .packed-switch 0x7f040035
        :pswitch_d
        :pswitch_8
        :pswitch_18
    .end packed-switch
.end method

.method public onTabReselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .registers 3
    .parameter "tab"
    .parameter "ft"

    .prologue
    .line 107
    return-void
.end method

.method public onTabSelected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .registers 5
    .parameter "tab"
    .parameter "ft"

    .prologue
    .line 113
    iget-object v0, p0, Lcom/ntu/converge/HomeActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 115
    return-void
.end method

.method public onTabUnselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .registers 3
    .parameter "tab"
    .parameter "ft"

    .prologue
    .line 121
    return-void
.end method
