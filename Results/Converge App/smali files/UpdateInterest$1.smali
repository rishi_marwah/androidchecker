.class Lcom/ntu/converge/UpdateInterest$1;
.super Ljava/lang/Object;
.source "UpdateInterest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ntu/converge/UpdateInterest;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/ntu/converge/UpdateInterest;

.field private final synthetic val$allInterestsIds:Ljava/util/ArrayList;

.field private final synthetic val$userid:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/ntu/converge/UpdateInterest;Ljava/util/ArrayList;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/ntu/converge/UpdateInterest$1;->this$0:Lcom/ntu/converge/UpdateInterest;

    iput-object p2, p0, Lcom/ntu/converge/UpdateInterest$1;->val$allInterestsIds:Ljava/util/ArrayList;

    iput-object p3, p0, Lcom/ntu/converge/UpdateInterest$1;->val$userid:Ljava/lang/String;

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 7
    .parameter "v"

    .prologue
    .line 96
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 97
    .local v2, interestUpdates:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, i:I
    :goto_6
    iget-object v3, p0, Lcom/ntu/converge/UpdateInterest$1;->val$allInterestsIds:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_16

    .line 106
    iget-object v3, p0, Lcom/ntu/converge/UpdateInterest$1;->this$0:Lcom/ntu/converge/UpdateInterest;

    iget-object v4, p0, Lcom/ntu/converge/UpdateInterest$1;->val$userid:Ljava/lang/String;

    #calls: Lcom/ntu/converge/UpdateInterest;->onUpdateButtonClicked(Ljava/util/ArrayList;Ljava/lang/String;)V
    invoke-static {v3, v2, v4}, Lcom/ntu/converge/UpdateInterest;->access$1(Lcom/ntu/converge/UpdateInterest;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 107
    return-void

    .line 99
    :cond_16
    iget-object v3, p0, Lcom/ntu/converge/UpdateInterest$1;->this$0:Lcom/ntu/converge/UpdateInterest;

    #getter for: Lcom/ntu/converge/UpdateInterest;->ll:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/ntu/converge/UpdateInterest;->access$0(Lcom/ntu/converge/UpdateInterest;)Landroid/widget/LinearLayout;

    move-result-object v4

    iget-object v3, p0, Lcom/ntu/converge/UpdateInterest$1;->val$allInterestsIds:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 100
    .local v0, cb:Landroid/widget/CheckBox;
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_3f

    .line 102
    iget-object v3, p0, Lcom/ntu/converge/UpdateInterest$1;->val$allInterestsIds:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    :cond_3f
    add-int/lit8 v1, v1, 0x1

    goto :goto_6
.end method
