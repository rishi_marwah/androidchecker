.class public Lcom/ntu/converge/UpdateInterest;
.super Landroid/app/Activity;
.source "UpdateInterest.java"


# instance fields
.field private ll:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/ntu/converge/UpdateInterest;)Landroid/widget/LinearLayout;
    .registers 2
    .parameter

    .prologue
    .line 22
    iget-object v0, p0, Lcom/ntu/converge/UpdateInterest;->ll:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1(Lcom/ntu/converge/UpdateInterest;Ljava/util/ArrayList;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 123
    invoke-direct {p0, p1, p2}, Lcom/ntu/converge/UpdateInterest;->onUpdateButtonClicked(Ljava/util/ArrayList;Ljava/lang/String;)V

    return-void
.end method

.method private onUpdateButtonClicked(Ljava/util/ArrayList;Ljava/lang/String;)V
    .registers 13
    .parameter
    .parameter "userid"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 125
    .local p1, updates:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v6, "UserInterests"

    invoke-static {v6}, Lcom/parse/ParseQuery;->getQuery(Ljava/lang/String;)Lcom/parse/ParseQuery;

    move-result-object v4

    .line 126
    .local v4, queryUserInterest:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    const-string v6, "user_id"

    invoke-virtual {v4, v6, p2}, Lcom/parse/ParseQuery;->whereEqualTo(Ljava/lang/String;Ljava/lang/Object;)Lcom/parse/ParseQuery;

    .line 127
    const/4 v5, 0x0

    .line 129
    .local v5, userInterests:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    :try_start_c
    invoke-virtual {v4}, Lcom/parse/ParseQuery;->find()Ljava/util/List;

    move-result-object v5

    .line 130
    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/parse/ParseObject;->deleteAllInBackground(Ljava/util/List;Lcom/parse/DeleteCallback;)V
    :try_end_14
    .catch Lcom/parse/ParseException; {:try_start_c .. :try_end_14} :catch_29

    .line 137
    :goto_14
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_18
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2e

    .line 144
    new-instance v1, Landroid/content/Intent;

    const-class v6, Lcom/ntu/converge/HomeActivity;

    invoke-direct {v1, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 145
    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/ntu/converge/UpdateInterest;->startActivity(Landroid/content/Intent;)V

    .line 147
    return-void

    .line 132
    .end local v1           #intent:Landroid/content/Intent;
    :catch_29
    move-exception v0

    .line 134
    .local v0, e1:Lcom/parse/ParseException;
    invoke-virtual {v0}, Lcom/parse/ParseException;->printStackTrace()V

    goto :goto_14

    .line 137
    .end local v0           #e1:Lcom/parse/ParseException;
    :cond_2e
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 138
    .local v3, interest_id:I
    new-instance v2, Lcom/parse/ParseObject;

    const-string v6, "UserInterests"

    invoke-direct {v2, v6}, Lcom/parse/ParseObject;-><init>(Ljava/lang/String;)V

    .line 139
    .local v2, interestUpdates:Lcom/parse/ParseObject;
    const-string v6, "chc"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    const-string v6, "user_id"

    invoke-virtual {v2, v6, p2}, Lcom/parse/ParseObject;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 141
    const-string v6, "interest_id"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v2, v6, v8}, Lcom/parse/ParseObject;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 142
    invoke-virtual {v2}, Lcom/parse/ParseObject;->saveInBackground()V

    goto :goto_18
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .registers 25
    .parameter "savedInstanceState"

    .prologue
    .line 26
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 27
    invoke-static {}, Lcom/ntu/converge/MainActivity;->getUserID()Ljava/lang/String;

    move-result-object v18

    .line 29
    .local v18, userid:Ljava/lang/String;
    new-instance v19, Landroid/widget/LinearLayout;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ntu/converge/UpdateInterest;->ll:Landroid/widget/LinearLayout;

    .line 31
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 33
    .local v8, currentInterests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v19, "UserInterests"

    invoke-static/range {v19 .. v19}, Lcom/parse/ParseQuery;->getQuery(Ljava/lang/String;)Lcom/parse/ParseQuery;

    move-result-object v15

    .line 34
    .local v15, queryUserInterest:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    const-string v19, "user_id"

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v15, v0, v1}, Lcom/parse/ParseQuery;->whereEqualTo(Ljava/lang/String;Ljava/lang/Object;)Lcom/parse/ParseQuery;

    .line 35
    const/16 v17, 0x0

    .line 37
    .local v17, userInterests:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    :try_start_2c
    invoke-virtual {v15}, Lcom/parse/ParseQuery;->find()Ljava/util/List;

    move-result-object v17

    .line 38
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_34
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z
    :try_end_37
    .catch Lcom/parse/ParseException; {:try_start_2c .. :try_end_37} :catch_ed

    move-result v20

    if-nez v20, :cond_d4

    .line 48
    :goto_3a
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 49
    .local v5, allInterestsNames:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 50
    .local v4, allInterestsIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v19, "Interests"

    invoke-static/range {v19 .. v19}, Lcom/parse/ParseQuery;->getQuery(Ljava/lang/String;)Lcom/parse/ParseQuery;

    move-result-object v14

    .line 53
    .local v14, queryAllInterest:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    :try_start_4a
    invoke-virtual {v14}, Lcom/parse/ParseQuery;->find()Ljava/util/List;

    move-result-object v3

    .line 54
    .local v3, Interests:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_52
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z
    :try_end_55
    .catch Lcom/parse/ParseException; {:try_start_4a .. :try_end_55} :catch_159

    move-result v20

    if-nez v20, :cond_f3

    .line 68
    .end local v3           #Interests:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    :goto_58
    new-instance v16, Landroid/widget/ScrollView;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 69
    .local v16, sv:Landroid/widget/ScrollView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ntu/converge/UpdateInterest;->ll:Landroid/widget/LinearLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x37

    invoke-virtual/range {v19 .. v20}, Landroid/widget/LinearLayout;->setId(I)V

    .line 70
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ntu/converge/UpdateInterest;->ll:Landroid/widget/LinearLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 72
    const-string v19, "all interests"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    const/4 v11, 0x0

    .local v11, i:I
    :goto_8e
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v19

    move/from16 v0, v19

    if-lt v11, v0, :cond_15f

    .line 86
    new-instance v6, Landroid/widget/Button;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 87
    .local v6, b:Landroid/widget/Button;
    const-string v19, "Update"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 88
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ntu/converge/UpdateInterest;->ll:Landroid/widget/LinearLayout;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 89
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ntu/converge/UpdateInterest;->ll:Landroid/widget/LinearLayout;

    move-object/from16 v19, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 92
    new-instance v19, Lcom/ntu/converge/UpdateInterest$1;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v4, v2}, Lcom/ntu/converge/UpdateInterest$1;-><init>(Lcom/ntu/converge/UpdateInterest;Ljava/util/ArrayList;Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/ntu/converge/UpdateInterest;->setContentView(Landroid/view/View;)V

    .line 112
    return-void

    .line 38
    .end local v4           #allInterestsIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v5           #allInterestsNames:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v6           #b:Landroid/widget/Button;
    .end local v11           #i:I
    .end local v14           #queryAllInterest:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    .end local v16           #sv:Landroid/widget/ScrollView;
    :cond_d4
    :try_start_d4
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/parse/ParseObject;

    .line 39
    .local v13, interests:Lcom/parse/ParseObject;
    const-string v20, "interest_id"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Lcom/parse/ParseObject;->getInt(Ljava/lang/String;)I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_eb
    .catch Lcom/parse/ParseException; {:try_start_d4 .. :try_end_eb} :catch_ed

    goto/16 :goto_34

    .line 43
    .end local v13           #interests:Lcom/parse/ParseObject;
    :catch_ed
    move-exception v10

    .line 45
    .local v10, e1:Lcom/parse/ParseException;
    invoke-virtual {v10}, Lcom/parse/ParseException;->printStackTrace()V

    goto/16 :goto_3a

    .line 54
    .end local v10           #e1:Lcom/parse/ParseException;
    .restart local v3       #Interests:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    .restart local v4       #allInterestsIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v5       #allInterestsNames:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v14       #queryAllInterest:Lcom/parse/ParseQuery;,"Lcom/parse/ParseQuery<Lcom/parse/ParseObject;>;"
    :cond_f3
    :try_start_f3
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/parse/ParseObject;

    .line 55
    .restart local v13       #interests:Lcom/parse/ParseObject;
    const-string v20, "Tag"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "name"

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Lcom/parse/ParseObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v22, "bhj,"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    const-string v20, "name"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Lcom/parse/ParseObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    const-string v20, "Tag"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "interest_id"

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Lcom/parse/ParseObject;->getInt(Ljava/lang/String;)I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v22, "bhj,"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    const-string v20, "interest_id"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Lcom/parse/ParseObject;->getInt(Ljava/lang/String;)I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_157
    .catch Lcom/parse/ParseException; {:try_start_f3 .. :try_end_157} :catch_159

    goto/16 :goto_52

    .line 61
    .end local v3           #Interests:Ljava/util/List;,"Ljava/util/List<Lcom/parse/ParseObject;>;"
    .end local v13           #interests:Lcom/parse/ParseObject;
    :catch_159
    move-exception v9

    .line 63
    .local v9, e:Lcom/parse/ParseException;
    invoke-virtual {v9}, Lcom/parse/ParseException;->printStackTrace()V

    goto/16 :goto_58

    .line 75
    .end local v9           #e:Lcom/parse/ParseException;
    .restart local v11       #i:I
    .restart local v16       #sv:Landroid/widget/ScrollView;
    :cond_15f
    new-instance v7, Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    .line 76
    .local v7, cb:Landroid/widget/CheckBox;
    invoke-virtual {v4, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 77
    .local v12, interestId:I
    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/CharSequence;

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 78
    invoke-virtual {v7, v12}, Landroid/widget/CheckBox;->setId(I)V

    .line 79
    const/high16 v19, 0x41f0

    move/from16 v0, v19

    invoke-virtual {v7, v0}, Landroid/widget/CheckBox;->setTextSize(F)V

    .line 80
    const-string v19, "checkbox"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "cb id : "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Landroid/widget/CheckBox;->getId()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_1b0

    .line 82
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v7, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 83
    :cond_1b0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ntu/converge/UpdateInterest;->ll:Landroid/widget/LinearLayout;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 74
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_8e
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/ntu/converge/UpdateInterest;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f090001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 120
    const/4 v0, 0x1

    return v0
.end method
