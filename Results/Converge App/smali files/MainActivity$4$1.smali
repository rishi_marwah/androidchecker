.class Lcom/ntu/converge/MainActivity$4$1;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Lcom/facebook/Request$GraphUserListCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ntu/converge/MainActivity$4;->onCompleted(Lcom/facebook/model/GraphUser;Lcom/facebook/Response;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/ntu/converge/MainActivity$4;

.field private final synthetic val$user:Lcom/facebook/model/GraphUser;


# direct methods
.method constructor <init>(Lcom/ntu/converge/MainActivity$4;Lcom/facebook/model/GraphUser;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/ntu/converge/MainActivity$4$1;->this$1:Lcom/ntu/converge/MainActivity$4;

    iput-object p2, p0, Lcom/ntu/converge/MainActivity$4$1;->val$user:Lcom/facebook/model/GraphUser;

    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Ljava/util/List;Lcom/facebook/Response;)V
    .registers 8
    .parameter
    .parameter "response"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/model/GraphUser;",
            ">;",
            "Lcom/facebook/Response;",
            ")V"
        }
    .end annotation

    .prologue
    .line 192
    .local p1, users:Ljava/util/List;,"Ljava/util/List<Lcom/facebook/model/GraphUser;>;"
    invoke-virtual {p2}, Lcom/facebook/Response;->getError()Lcom/facebook/FacebookRequestError;

    move-result-object v2

    if-nez v2, :cond_10

    .line 194
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_11

    .line 203
    :cond_10
    return-void

    .line 194
    :cond_11
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/model/GraphUser;

    .line 195
    .local v1, usr:Lcom/facebook/model/GraphUser;
    new-instance v0, Lcom/parse/ParseObject;

    const-string v3, "Friends"

    invoke-direct {v0, v3}, Lcom/parse/ParseObject;-><init>(Ljava/lang/String;)V

    .line 197
    .local v0, frndlist:Lcom/parse/ParseObject;
    const-string v3, "user_id"

    iget-object v4, p0, Lcom/ntu/converge/MainActivity$4$1;->val$user:Lcom/facebook/model/GraphUser;

    invoke-interface {v4}, Lcom/facebook/model/GraphUser;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/parse/ParseObject;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 198
    const-string v3, "friend_id"

    invoke-interface {v1}, Lcom/facebook/model/GraphUser;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/parse/ParseObject;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 199
    invoke-virtual {v0}, Lcom/parse/ParseObject;->saveInBackground()V

    goto :goto_a
.end method
