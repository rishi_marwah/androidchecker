import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.*;

public class Block {

	// static hashmap for each method call
	static public HashMap<String, ArrayList<String>> methodCallMap = new HashMap<String, ArrayList<String>>();
	static public HashMap<String, ArrayList<String>> fullMethodCallMap = new HashMap<String, ArrayList<String>>();

	private String parent_class;
	private String parent_method;
	private String label;
	private ArrayList<String> instructions;
	private HashMap<String, String> target;

	public Block() {
		// default constructor
		/*
		 * parent_class: Class where our code is located. parent_method: Class
		 * method where our code is located. label: Block identifier (class
		 * name[space]method name[space]first line offset). instructions: list
		 * raw instructions targets: Code flow changes targets, if any.
		 */
		target = new HashMap<String, String>();
		instructions = new ArrayList<String>();
		parent_class = null;
		label = null;
		parent_method = null;
	}

	@SuppressWarnings("unchecked")
	public Block(Block blk) {
		// copy constructor
		// set targets to null as those we will change when reversing the graph
		target = new HashMap<String, String>();
		instructions = (ArrayList<String>) blk.instructions.clone();
		parent_class = blk.parent_class;
		label = blk.label;
		parent_method = blk.parent_method;
	}

	public Block(String parent_class, String parent_method, String label,
			ArrayList<String> instructions) {
		// Constructor
		this.parent_class = parent_class;
		this.parent_method = parent_method;
		this.label = label;
		this.instructions = instructions;
		this.target = new HashMap<String, String>();
	}

	public void setTarget(String edge_label, String target_label) {
		// Add the edge label for graph and its target block in graph to the
		// current block
		target.put(edge_label, target_label);
	}

	public void setMethod(String method) {
		this.parent_method = method;
	}

	public void setClass(String cls) {
		this.parent_class = cls;
	}

	public void SetLabel(String lbl) {

		this.label = lbl;
	}

	public String getMethodName() {
		// Return method
		return parent_method;
	}

	public String getClassName() {
		// Return class
		return parent_class;
	}

	public String getLabel() {
		// Return label
		return label;
	}

	public HashMap<String, String> getTarget() {
		// Return label
		return target;
	}

	public int getLength() {
		// Return instructions length
		return instructions.size();
	}

	public ArrayList<String> getInstructions() {
		// Return instructions length
		return instructions;
	}

	public boolean isTag(String inst, String tag) {
		// Matches the instructions to check if its a known instruction
		Matcher m;
		if (tag.equals("isMethodBegin")) {

			Pattern regex = Pattern.compile("^.method.+");
			m = regex.matcher(inst);
		}

		else if (tag.equals("isMethodEnd")) {

			Pattern regex = Pattern.compile("^.end method.+");
			m = regex.matcher(inst);
		}

		else if (tag.equals("isJump")) {

			Pattern regex = Pattern.compile("^goto.+");
			m = regex.matcher(inst);
		}

		else if (tag.equals("isConditional")) {

			Pattern regex = Pattern.compile("^if-.+");
			m = regex.matcher(inst);
		}

		else if (tag.equals("isCatch")) {

			Pattern regex = Pattern.compile("^.catch.+");
			m = regex.matcher(inst);
		}

		else if (tag.equals("isTryStart")) {

			Pattern regex = Pattern.compile("^\\:try_start_.+");
			m = regex.matcher(inst);
		}

		else if (tag.equals("isLabel")) {

			Pattern regex = Pattern.compile("^\\:.+");
			// is this valid regex? escape ':' required?
			// Original author comment :
			// match = re.search("^\:" , inst) and ((not re.search("^\:try" ,
			// inst)) and (not re.search("^\:catch" , inst)))
			m = regex.matcher(inst);
		}

		else if (tag.equals("isReturn")) {

			Pattern regex = Pattern.compile("^return.+");
			// check to see if only return is ok instead of return-
			m = regex.matcher(inst);
		}

		else if (tag.equals("isSwitchStart")) {

			Pattern regex = Pattern
					.compile("^(.+?)\\-switch(.+?):(.+)switch_(.+)");
			// check to see if only return is ok instead of return-
			m = regex.matcher(inst);
		}

		else if (tag.equals("isCall")) {

			Pattern regex = Pattern.compile("^invoke-.+");
			m = regex.matcher(inst);

			// at every method call put the method and parent class
			// and calling method in hashmap

			if (m.matches()) {

				// getting the full classname then trimming it to only the name
				// of class
				// for search of the method the user inputted
				// otherwise search for full class name
				// same for methodname
				// two hashmaps one for search of inputted method other for
				// normal invokes
				Matcher cname = Pattern.compile("(^.+)\\,\\ (.+);->(.+)")
						.matcher(inst);

				cname.find();

				String classname = cname.group(2);
				String fullmethodname = cname.group(3);
				String method = fullmethodname.split("\\(")[0];

				String keyFull = classname + " " + fullmethodname;

				String[] classn = classname.split("\\/");

				String cls = classn[classn.length - 1];
				String key = cls + " " + method;

				if (!methodCallMap.containsKey(key)) {
					// add to hashmap
					ArrayList<String> methodCalls = new ArrayList<String>();
					methodCalls.add(this.label);

					methodCallMap.put(key, methodCalls);
				} else {
					ArrayList<String> methodCalls = methodCallMap.get(key);
					// check for duplicate and add
					if (!methodCalls.contains(this.label)) {
						methodCalls.add(this.label);
					}
				}

				// second hashmap for full method
				if (!fullMethodCallMap.containsKey(keyFull)) {
					// add to hashmap
					ArrayList<String> methodCalls = new ArrayList<String>();
					methodCalls.add(this.label);

					fullMethodCallMap.put(keyFull, methodCalls);
				} else {
					ArrayList<String> methodCalls = fullMethodCallMap
							.get(keyFull);
					// check for duplicate and add
					if (!methodCalls.contains(this.label)) {
						methodCalls.add(this.label);
					}
				}
			}
		}

		else
			m = null;

		if (m.matches())
			return true;
		else
			return false;
	}

	// special switch jump method to deal with duplicate statements for the
	// switch case smali
	public boolean isSwitchJump(String inst, int n, ArrayList<String> insts) {

		Matcher m1, m2;
		Pattern regex = Pattern.compile("^(.*)\\:(.+?)switch_.+");
		Pattern switchEnd = Pattern.compile("^\\.end (.+?)switch");
		m1 = regex.matcher(inst);
		if (n >= insts.size() || !m1.matches())
			return false;

		int next = n + 1;
		String nextInst = insts.get(next);

		m2 = switchEnd.matcher(nextInst);
		// Recursive function
		// if next line is an .end switch or a switch jump then current is also
		// one
		if (m2.matches())
			return true;

		boolean isNextTrue = this.isSwitchJump(nextInst, next, insts);
		if (isNextTrue)
			return true;

		return false;
	}

	public void addInstruction(String inst) {
		// add Instructions to the list
		instructions.add(inst);
	}

	public boolean isNeighbor(Block b) {
		// check if the current and parameter have same parent class and method
		if (this.parent_class.equals(b.getClassName())
				&& this.parent_method.equals(b.getMethodName()))
			return true;
		else
			return false;
	}

	public boolean hasString(String method) {
		// if instructions array has contains string and its not a comment
		// return true
		for (int i = 0; i < this.instructions.size(); i++) {
			if (instructions.get(i).contains(method)
					&& !instructions.get(i).startsWith("#")) {
				return true;
			}
		}

		return false;
	}

	// Use the returned string as the label in the graph
	// Convert the "" to ''
	public String getGraphNodeLabel() {
		StringBuffer node_label = new StringBuffer();
		node_label.append(this.getLabel() + "\n");

		for (int i = 0; i < this.getLength(); i++) {
			node_label.append(this.instructions.get(i) + "\n");
		}

		return node_label.toString().replace("\"", "'");
	}
}
