import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainClass {

	// !! NOTES Fixed switch and loop bugs
	// Can be used in Mac/UNIX only coz of paths
	// See if dot location determined programmatically
	// See if the .catch .{try_start .. try_end } is required
	// If else works
	// Check if the .line was needed
	// Added hashmap for faster searching
	// Removed # comments searching !
	// ! Link multiple classes !!
	// ! All classes needed to be reversed?
	// ! Draw final graph in one image?
	// ! How to end at user input
	// ! labels for extended path
	// ! Duplicate reverse graph for method hashmap in search
	// ! Only methodname to its calling block is mapped in Block hashmap

	// Hashmap for the mapping from class to index in progamBlocks
	public static HashMap<String, Integer> mp = new HashMap<String, Integer>();

	// blocks list for all classes
	public static ArrayList<Blocks> programBlocks = new ArrayList<Blocks>();

	// counter to avoid duplicate graph names
	public static int counter = 0;

	// set for user input methods
	// using existing knowledge of smali event listeners
	// list of triggers from
	// http://developer.android.com/guide/topics/ui/ui-events.html
	public static HashSet<String> userInputs = new HashSet<String>();

	// main method
	public static void main(String args[]) {
		double Tstart = System.nanoTime();
		// Main method execution

		// checking for valid inputs and actions required
		boolean isValidClassInput = false;
		boolean isValidFileInput = false;
		boolean searchRequired = false;
		boolean drawInputGraphs = false;

		// input from user
		String classpath = null;
		String filepath = null;
		String methods = null;
		String[] methodnames = null;

		// file path for all classes in a folder
		ArrayList<String> filepaths = new ArrayList<String>();

		// reversed blocks for all classes or 1 class
		// ArrayList<Blocks> programBlocksReversed = new ArrayList<Blocks>();

		// list of the blocks containing the given method

		userInputs.add("onClick");
		userInputs.add("onLongClick");
		userInputs.add("onFocusChange");
		userInputs.add("onKey");
		userInputs.add("onTouch");

		ArrayList<Blocks> matchedBlocks = new ArrayList<Blocks>();

		for (int i = 0; i < args.length - 1; i++) {
			// if -c is present and next input is not empty
			if (args[i].equals("-c") && !args[i + 1].isEmpty()) {
				classpath = args[i + 1];
				isValidClassInput = true;
			}

			else if (args[i].equals("-f") && !args[i + 1].isEmpty()) {
				filepath = args[i + 1];
				isValidFileInput = true;
			}

			else if (args[i].equals("-m") && !args[i + 1].isEmpty()) {
				methods = args[i + 1];
				searchRequired = true;
			}

			else if (args[i].equals("-o") && !args[i + 1].isEmpty()) {
				if (args[i + 1].toLowerCase().equals("true")) {
					drawInputGraphs = true;
				}
			}
		}

		if (!(isValidClassInput || isValidFileInput)) {
			System.out
					.println("Execute with -c followed by classname with path or "
							+ "-f with path of folder as argument!");
			double Tend = System.nanoTime();
			printTime(Tstart, Tend, "Total time taken");
			return;
		}

		// folder given higher priority than class
		else if (isValidFileInput) {

			// making filepath uniform
			if (!filepath.endsWith("/"))
				filepath = filepath + "/";

			File file = new File(filepath);

			if (!file.exists()) {
				System.out.println("This folder doesnt exist");
				double Tend = System.nanoTime();
				printTime(Tstart, Tend, "Total time taken");
				return;
			}

			// get all smali files in folder
			listFilesForFolder(file, filepaths);

			String foldername = filepath + "Input Graphs/";

			// if output required then create folder else skip
			if (drawInputGraphs) {
				if (!createFolder(foldername)) {
					double Tend = System.nanoTime();
					printTime(Tstart, Tend, "Total time taken");
					return;
				}
				System.out.println("Input graphs will be generated at "
						+ foldername);
			}

			// generate blocks for each class name found in folder
			for (String className : filepaths) {
				Blocks blks = generateGraph(className, foldername,
						drawInputGraphs);
				if (blks != null) {
					programBlocks.add(blks);
					mp.put(blks.className, programBlocks.size() - 1);
				}
			}
		}

		else if (isValidClassInput) {

			Matcher cnames = Pattern.compile("(\\/.+\\/)(.+?)\\.(.+?)$")
					.matcher(classpath);
			// linear pass to build graph using graphing library

			if (!cnames.find()) {
				System.out.println("Wrong class input");
				double Tend = System.nanoTime();
				printTime(Tstart, Tend, "Total time taken");
				return;
			}

			String pathname = cnames.group(1);

			String foldername = pathname + "Input Graphs/";

			if (drawInputGraphs) {
				if (!createFolder(foldername)) {
					double Tend = System.nanoTime();
					printTime(Tstart, Tend, "Total time taken");
					return;
				}
				System.out.println("Input graphs will be generated at "
						+ foldername);
			}

			Blocks blks = generateGraph(classpath, foldername, drawInputGraphs);

			if (blks != null) {
				programBlocks.add(blks);
				mp.put(blks.className, programBlocks.size() - 1);
				filepath = blks.classPath;
			} else {
				System.out.println("Empty file");
				double Tend = System.nanoTime();
				printTime(Tstart, Tend, "Total time taken");
				return;
			}

		}

		if (searchRequired) {

			// get classname and method name from input

			methodnames = methods.split(",");

			boolean matchFound = false;

			for (String methodname : methodnames) {

				// error checking

				if (!methodname.contains(".")) {
					System.out
							.println("\nPlease enter method names without spaces in between"
									+ "\nEnsure that the methods follow the classname.methodname format");
					continue;
				}

				String[] methodarr = methodname.split("\\.");
				// using fully qualified name
				String classn = methodarr[0];
				String method = methodarr[1];

				String fullmethodname = classn + " " + method;
				// print time and search method exception
				// interaction among blocks
				matchedBlocks = search(fullmethodname);
				// if any blocks is returned

				if (matchedBlocks != null) {
					
					if (!createFolder(filepath + "Output Graphs/")) {
						double Tend = System.nanoTime();
						printTime(Tstart, Tend, "Total time taken");
						return;
					}
					matchFound = true;
					String outfoldername = filepath + "Output Graphs/"
							+ fullmethodname + "/";

					if (!createFolder(outfoldername)) {
						double Tend = System.nanoTime();
						printTime(Tstart, Tend, "Total time taken");
						return;
					}

					// search for backward path till user input or onCreate
					matchedBlocks = getBackwardsPath(matchedBlocks);

					for (Blocks matchedBlock : matchedBlocks) {
						// draw matched blocks at the given output file path

						// drawGraph(matchedBlock, matchedBlock.className
						// + " matched blocks", outfoldername);

						getExtendedPath(matchedBlock);

						drawGraph(matchedBlock, matchedBlock.className
								+ " matched blocks extended", outfoldername);
					}

					System.out.println("\nAll paths calling " + fullmethodname
							+ "() outputted at " + outfoldername + "\n");
				} else {
					System.out.println("\nNo path of method: " + fullmethodname
							+ "() found in this program \n");
				}
			}
			if (!matchFound) {
				System.out.println("\nNone of the inputted methods are"
						+ " called in this program \n");
			}
		}

		double Tend = System.nanoTime();
		printTime(Tstart, Tend, "Total time taken");

	}

	// appends the current path to the path that calls the first method
	public static ArrayList<Blocks> getBackwardsPath(
			ArrayList<Blocks> matchedBlocks) {
		double start = System.nanoTime();

		LinkedList<Blocks> queue = new LinkedList<Blocks>();
		for (Blocks matchedBlock : matchedBlocks) {
			// method name from first block
			queue.add(matchedBlock);
		}

		matchedBlocks = new ArrayList<Blocks>();

		// on each pass either a path is appended to the existing paths
		// or just removed
		while (!queue.isEmpty()) {
			Blocks matchedBlock = queue.getFirst();
			matchedBlock = getBackwardsPathRecursive(matchedBlock, queue);
			if (matchedBlock != null) {
				matchedBlocks.add(matchedBlock);
			}
			queue.remove();
		}

		double end = System.nanoTime();
		printTime(start, end, "Backward path for all classes generated in");

		return matchedBlocks;
	}

	// recursive call adds the calling method to path and appends it to the
	// queue
	public static Blocks getBackwardsPathRecursive(Blocks matchedBlock,
			LinkedList<Blocks> matchedBlocks) {

		String fullmethod = matchedBlock.blocks.get(0).getMethodName();
		String classn = matchedBlock.blocks.get(0).getClassName();

		// stop on on create
		String method = fullmethod.split("\\(")[0];
		if (method.equals("onCreate")) {
			return matchedBlock;
		}
		// stop on user input
		else if (userInputs.contains(method) && classn.contains("$")) {
			return matchedBlock;
		}

		String key = classn + " " + fullmethod;
		if (Block.fullMethodCallMap.containsKey(key)) {
			ArrayList<String> matches = Block.fullMethodCallMap.get(key);

			for (String label : matches) {
				String classname = label.split(" ")[0];
				Blocks blks = getBlocksByClassName(classname);
				blks = reverseGraph(blks);
				Block b = blks.getBlockFromLabel(label);

				// stop execution if block not found for some reason
				if (b == null) {
					return matchedBlock;
				}

				Blocks matched = getCopyPath(b, blks);
				matched = reverseGraph(matched);

				b = matched.getBlockFromLabel(label);

				// stop execution if block not found for some reason
				if (b == null) {
					return matchedBlock;
				}

				String target = classn + " " + fullmethod + " 1";

				// make a copy of the original in the new block
				for (Block blk : matchedBlock.blocks) {
					matched.add(blk);
				}

				b.setTarget("onCall", target);

				matchedBlocks.add(matched);
			}

			return null;
		} else {
			return matchedBlock;
		}
	}

	// incorporate path of the called methods if the exist into existing
	public static void getExtendedPath(Blocks matchedBlock) {

		double start = System.nanoTime();
		// only till size of original unmodified path since list is modified
		// later
		int blkSize = matchedBlock.blocks.size();

		for (int i = 0; i < blkSize; i++) {
			Block block = matchedBlock.blocks.get(i);

			// since method call always last inst
			int ind = block.getLength() - 1;
			String inst = block.getInstructions().get(ind);
			if (block.isTag(inst, "isCall")) {
				// get classname and method name from method call
				Matcher cname = Pattern.compile("(^.+)\\,\\ (.+);->(.+)")
						.matcher(inst);

				// linear pass to build graph using graphing library
				cname.find();

				String classname = cname.group(2);
				String methodname = cname.group(3);

				// get the blocks of the class which contains required
				// method

				Blocks blks = getBlocksByClassName(classname);
				if (blks != null) {
					String label = classname + " " + methodname + " 1";

					if (matchedBlock.mp.containsKey(label)) {
						continue;
					}

					Block b = blks.getBlockFromLabel(label);

					// if method not present exit current loop
					if (b == null) {
						continue;
					}

					Blocks matched = getCopyPath(b, blks);

					for (Block blk : matched.blocks) {
						matchedBlock.add(blk);
					}
					// set first block as target for current block in matched
					// path
					String initLabel = matched.blocks.get(0).getLabel();
					block.setTarget("onCall", initLabel);

					// set the ret of the first path as the target for the
					// return block
					Block ret = getReturnBlock(matched.blocks);

					String[] keys = block.getLabel().split(" ");
					String key = "On Return " + keys[keys.length - 2] + " "
							+ keys[keys.length - 1];
					String retLabel = block.getTarget().get(key);
					if (ret != null) {
						ret.setTarget("On Return External ", retLabel);
					}

				}

			}
		}

		double end = System.nanoTime();
		printTime(start, end, matchedBlock.className
				+ " extended path generated in");

	}

	// returns the Blocks from program blocks given the class to be searched
	public static Blocks getBlocksByClassName(String classname) {
		if (mp.containsKey(classname)) {
			int index = mp.get(classname);
			Blocks blks = programBlocks.get(index);
			return blks;
		}

		return null;
	}

	// get the block which contains the return statement to tie the 2 paths
	public static Block getReturnBlock(ArrayList<Block> blocks) {
		for (int i = 0; i < blocks.size(); i++) {
			Block b = blocks.get(i);
			// checking only last instruction of each block
			int ind = b.getLength() - 1;
			String inst = b.getInstructions().get(ind);
			if (b.isTag(inst, "isReturn")) {
				return b;
			}
		}

		return null;
	}

	// get whole path of block given the initial starting point
	public static Blocks getCopyPath(Block block, Blocks blocks) {
		HashSet<String> checked = new HashSet<String>();
		LinkedList<Block> blockQueue = new LinkedList<Block>();

		Blocks cpyList = new Blocks();

		blockQueue.add(block);

		while (!blockQueue.isEmpty()) {
			getCopyPathRecursive(blocks, checked, cpyList, blockQueue);
			blockQueue.remove();
		}

		// counter to ensure that same classes dont generate duplicates
		cpyList.className = blocks.className + " " + counter++;
		cpyList.classPath = blocks.classPath;

		return cpyList;
	}

	// method which recursively computes the path
	public static void getCopyPathRecursive(Blocks blocks,
			HashSet<String> checked, Blocks cpylist,
			LinkedList<Block> blockQueue) {

		Block block = blockQueue.getFirst();

		String vertexLabel = block.getLabel();
		if (checked.contains(vertexLabel)) {
			return;
		}

		else {
			checked.add(vertexLabel);
			Block cpy = new Block(block);

			Iterator<Entry<String, String>> iter = block.getTarget().entrySet()
					.iterator();
			while (iter.hasNext()) {
				// Iterate
				Map.Entry<String, String> pairs = (Map.Entry<String, String>) iter
						.next();
				String target_label = (String) pairs.getValue();
				String edge_label = (String) pairs.getKey();

				// add the targets for copy set and add to queue for target
				// blocks
				cpy.setTarget(edge_label, target_label);

				Block block2 = blocks.getBlockFromLabel(target_label);
				if (block2 != null) {
					blockQueue.add(block2);
				}
			}

			cpylist.add(cpy);
		}

	}

	// prints the time + message
	public static void printTime(double start, double end, String message) {
		// formatter for time output
		NumberFormat formatter = new DecimalFormat("#0.000000");
		double timeSpan = (double) (end - start) * .000000001;
		System.out.println(message + " " + formatter.format(timeSpan)
				+ " seconds");
	}

	// generates a block for the given class and draws its graph also
	public static Blocks generateGraph(String className, String foldername,
			boolean drawInputGraphs) {
		// linear pass to split into smaller instruction blocks
		// and set their target locations
		double start = System.nanoTime();

		Blocks blocks = setInstructionTargets(className);
		// Extracting name and path
		if (blocks == null) {
			return null;
		}

		// get path from input value
		Matcher cname = Pattern.compile("^(\\/.+\\/)*(.+)\\.(.+)$").matcher(
				className);
		// linear pass to build graph using graphing library
		cname.find();

		// full class name from its first block
		String classname = blocks.blocks.get(0).getClassName();
		String pathname = cname.group(1);

		blocks.className = classname;
		blocks.classPath = pathname;

		if (drawInputGraphs) {
			drawGraph(blocks, classname, foldername);
		}

		double end = System.nanoTime();
		printTime(start, end, blocks.className + " generated in");

		return blocks;
	}

	// create new folder
	public static boolean createFolder(String foldername) {
		File file = new File(foldername);
		if (!file.exists()) {
			if (!file.mkdir()) {
				System.out.println("Failed to create " + foldername
						+ " directory!");
				return false;
			}
		}
		return true;

	}

	// calls the recursive method that reverses a given block of instructions
	public static Blocks reverseGraph(Blocks blocks) {
		double start = System.nanoTime();
		// reversed graph to extract path
		HashSet<String> checked = new HashSet<String>();
		Blocks reverse = new Blocks();

		// 1 for loop for all vertices
		for (int i = 0; i < blocks.blocks.size(); i++) {
			Block block = blocks.blocks.get(i);
			// recursive method to reverse each unvisited node
			reverseGraphRecursive(reverse, blocks, block.getLabel(), checked);
		}

		reverse.className = blocks.className;
		reverse.classPath = blocks.classPath;

		// drawGraph(reverse, reverse.className + " reverse",
		// reverse.classPath);

		double end = System.nanoTime();
		printTime(start, end, reverse.className + " reversed in");

		return reverse;
	}

	// http://stackoverflow.com/questions/15084010/reverse-the-directed-graph
	public static void reverseGraphRecursive(Blocks copy, Blocks orig,
			String vertexLabel, HashSet<String> checked) {

		// Block block1 =
		if (checked.contains(vertexLabel)) {
			return;
		}

		checked.add(vertexLabel);
		Block bl = copy.getBlockFromLabel(vertexLabel);
		if (bl == null) {
			bl = orig.getBlockFromLabel(vertexLabel);
			Block b = new Block(bl);
			copy.add(b);
		}
		LinkedList<String> ll = new LinkedList<String>();

		// Iterate over the hashmap of targets
		Block currentBlock = orig.getBlockFromLabel(vertexLabel);
		Iterator<Entry<String, String>> iter = currentBlock.getTarget()
				.entrySet().iterator();

		while (iter.hasNext()) {
			Map.Entry<String, String> pairs = (Map.Entry<String, String>) iter
					.next();
			String edge_label = (String) pairs.getKey();
			if (edge_label.isEmpty())
				edge_label = "Cont";
			String target_label = (String) pairs.getValue();

			Block blk = copy.getBlockFromLabel(target_label);

			if (blk != null) {
				blk.setTarget(edge_label, vertexLabel);
			} else {
				blk = orig.getBlockFromLabel(target_label);
				Block b = new Block(blk);
				b.setTarget(edge_label, vertexLabel);
				copy.add(b);
			}

			if (!checked.contains(target_label)) {
				ll.add(target_label);
			}
		}

		ListIterator<String> it = ll.listIterator();
		while (it.hasNext())
			reverseGraphRecursive(copy, orig, it.next(), checked);
		return;
	}

	// extracts all files from a folder recursively and adds to an ArrayList
	public static void listFilesForFolder(final File folder,
			ArrayList<String> filepaths) {
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				listFilesForFolder(fileEntry, filepaths);
			} else {
				// extract only smali files
				if (fileEntry.getName().endsWith(".smali")) {
					// add each file to the ArrayList
					// System.out.println(fileEntry.getAbsolutePath());
					filepaths.add(fileEntry.getAbsolutePath());
				}
			}
		}
	}

	// breaks method into blocks and sets the target instruction for each block
	public static Blocks setInstructionTargets(String filepath) {
		// linear pass over instructions to set each blocs target

		ArrayList<InstructionSet> out_inst = Blocks.extractBlocks(filepath);

		if (out_inst == null) {
			return null;
		}
		// linear pass 1
		// split method code block into smaller blocks
		// (calls/jumps/conditionals/labeled/catch)
		// calculate block target/s.
		Blocks blocks = new Blocks();
		// default new block
		for (int i = 0; i < out_inst.size(); i++) {

			String class_name = out_inst.get(i).class_name;
			String method_name = out_inst.get(i).method_name;

			String label = out_inst.get(i).class_name + " "
					+ out_inst.get(i).method_name + " 1";

			// first block always label 1

			ArrayList<String> insts = new ArrayList<String>();
			Block b = new Block(out_inst.get(i).class_name,
					out_inst.get(i).method_name, label, insts);
			ArrayList<String> method_inst = out_inst.get(i).instructions;

			for (int j = 0; j < method_inst.size(); j++) {
				// Extracting number from label
				String curr_inst = method_inst.get(j);
				String temp_arr[] = b.getLabel().split(" ");
				int blockPos = Integer.parseInt(temp_arr[temp_arr.length - 1]);
				// current block position
				if (b.isSwitchJump(curr_inst, j, method_inst)) {
					// if jump condition find target and add then move to next
					// instruction and increment label
					// Very similar to label
					ArrayList<String> target_label = Blocks.splitBlock(b,
							class_name, method_name, blockPos, 0, method_inst,
							curr_inst);
					String incLabel = target_label.get(0);
					String posLabel = target_label.get(1);
					b = blocks.addAfter(incLabel, curr_inst, b, class_name,
							method_name);
					b.setTarget("Switch " + method_name + " " + blockPos,
							posLabel);
				}

				else if (b.isTag(curr_inst, "isJump")) {
					// if jump condition find target and add then move to next
					// instruction and increment label
					ArrayList<String> target_label = Blocks.splitBlock(b,
							class_name, method_name, blockPos, 1, method_inst,
							curr_inst);
					String incLabel = target_label.get(0);
					String posLabel = target_label.get(1);
					b.setTarget("Jump" + " " + method_name + " " + blockPos,
							posLabel);
					b = blocks.addBefore(incLabel, curr_inst, b, class_name,
							method_name);
				}

				else if (b.isTag(curr_inst, "isConditional")) {
					ArrayList<String> target_label = Blocks.splitBlock(b,
							class_name, method_name, blockPos, 1, method_inst,
							curr_inst);
					String incLabel = target_label.get(0);
					String posLabel = target_label.get(1);
					// skips to :if on true otherwise continues for false
					b.setTarget("True" + " " + method_name + " " + blockPos,
							posLabel);
					b.setTarget("False" + " " + method_name + " " + blockPos,
							incLabel);
					b = blocks.addBefore(incLabel, curr_inst, b, class_name,
							method_name);
				}
				// if tryStart then jump to either exception or try
				else if (b.isTag(curr_inst, "isTryStart")) {
					ArrayList<String> target_label = Blocks.splitBlock(b,
							class_name, method_name, blockPos, 1, method_inst,
							curr_inst);
					String incLabel = target_label.get(0);
					String posLabel = target_label.get(1);
					b.setTarget("Exception" + " " + method_name + " "
							+ blockPos, posLabel);
					b.setTarget("Try" + " " + method_name + " " + blockPos,
							incLabel);
					b = blocks.addBefore(incLabel, curr_inst, b, class_name,
							method_name);
				}
				// Switch
				else if (b.isTag(curr_inst, "isSwitchStart")) {
					ArrayList<String> target_label = Blocks.splitBlock(b,
							class_name, method_name, blockPos, 1, method_inst,
							curr_inst);
					String incLabel = target_label.get(0);
					// for all switch target labels add to the target hashmap in
					// block
					for (int index = 1; index < target_label.size(); index++) {
						String posLabel = target_label.get(index);
						b.setTarget("Matched Case " + index + " " + method_name
								+ " " + blockPos, posLabel);
					}

					b.setTarget("Deafult Switch Case" + " " + method_name + " "
							+ blockPos, incLabel);
					b = blocks.addBefore(incLabel, curr_inst, b, class_name,
							method_name);
				}

				else if (b.isTag(curr_inst, "isLabel")) {
					// Current instruction not getting added in the block
					// New block created and label added
					// Label condition to split the ':*' into its own block for
					// targets of others
					ArrayList<String> target_label = Blocks.splitBlock(b,
							class_name, method_name, blockPos, 0, method_inst,
							curr_inst);
					String incLabel = target_label.get(0);
					b.setTarget("Cont" + " " + method_name + " " + blockPos,
							incLabel);
					b = blocks.addAfter(incLabel, curr_inst, b, class_name,
							method_name);
				}

				else if (b.isTag(curr_inst, "isReturn")) {
					ArrayList<String> target_label = Blocks.splitBlock(b,
							class_name, method_name, blockPos, 1, method_inst,
							curr_inst);
					String incLabel = target_label.get(0);
					b = blocks.addBefore(incLabel, curr_inst, b, class_name,
							method_name);
				}

				else if (b.isTag(curr_inst, "isCall")) {
					ArrayList<String> target_label = Blocks.splitBlock(b,
							class_name, method_name, blockPos, 1, method_inst,
							curr_inst);
					String incLabel = target_label.get(0);
					String posLabel = target_label.get(1);
					b.setTarget("On Return" + " " + method_name + " "
							+ blockPos, posLabel);
					b = blocks.addBefore(incLabel, curr_inst, b, class_name,
							method_name);
				}

				else {
					// default case
					b.addInstruction(curr_inst);

				}
			}

			blocks.add(b);
		}

		return blocks;
	}

	// returns all the paths to reach passed method if it exists else null
	public static ArrayList<Blocks> search(String methodname) {
		// updated from linear search to extract from hashmap
		// one distinct path per call even if same class

		ArrayList<String> matched = new ArrayList<String>();

		if (!Block.methodCallMap.containsKey(methodname)) {
			return null;
		} else {
			matched = Block.methodCallMap.get(methodname);
		}

		// search the hashmap for method calls then generate a distinct path for
		// all calls

		if (!matched.isEmpty()) {
			ArrayList<Blocks> matchedBlocks = new ArrayList<Blocks>();

			for (String label : matched) {
				String classname = label.split(" ")[0];
				Blocks blks = getBlocksByClassName(classname);
				Blocks reverse = reverseGraph(blks);
				Blocks pathR = getPathFromGraph(label, reverse);
				// reverse again to get path from original path
				if (pathR == null) {
					continue;
				}
				Blocks path = reverseGraph(pathR);

				// returns the path from the first intruction to block which
				// calls the instructions
				matchedBlocks.add(path);
			}
			return matchedBlocks;
		}

		return null;
	}

	// iterative graph drawing method
	public static void drawGraph(Blocks b, String classname, String pathname) {
		GraphManager g = new GraphManager();

		for (int i = 0; i < b.blocks.size(); i++) {
			Block block = b.blocks.get(i);

			// Iterate over the hashmap of targets if not empty
			// also check if block is not only ".end method"
			if (block.getTarget().isEmpty() && !block.hasString(".end method")) {
				g.addNode(block);
			}
			Iterator<Entry<String, String>> iter = block.getTarget().entrySet()
					.iterator();
			while (iter.hasNext()) {
				// Iterate
				Map.Entry<String, String> pairs = (Map.Entry<String, String>) iter
						.next();
				String edge_label = (String) pairs.getKey();
				if (edge_label.isEmpty())
					edge_label = "Cont";
				String target_label = (String) pairs.getValue();
				Block block2 = b.getBlockFromLabel(target_label);

				if (block2 != null) {
					g.addEdge(block, block2, edge_label);
				}
			}
		}
		// Filename of image and save
		g.draw(classname, pathname);

	}

	// create a path for the matched methods
	// similar to draw recursive without IO
	public static Blocks getPathFromGraph(String matchedLabel, Blocks reversed) {

		double start = System.nanoTime();

		Block b = reversed.getBlockFromLabel(matchedLabel);
		HashSet<String> checked = new HashSet<String>();
		LinkedList<Block> blockQueue = new LinkedList<Block>();

		if (b != null) {
			blockQueue.add(b);

		}

		else {
			return null;
		}

		Blocks path = new Blocks();

		while (!blockQueue.isEmpty()) {
			getPathFromGraphRecursive(blockQueue, checked, reversed, path);
			blockQueue.remove();
		}

		path.className = reversed.className + " Matched " + counter++;
		path.classPath = reversed.classPath;

		double end = System.nanoTime();
		printTime(start, end, path.className + " path generated in");

		return path;
	}

	// actual recursive method that creates a 'blocks' for each path
	public static void getPathFromGraphRecursive(LinkedList<Block> blockQueue,
			HashSet<String> checked, Blocks reversed, Blocks path) {
		Block block = blockQueue.getFirst();

		String vertexLabel = block.getGraphNodeLabel();
		if (checked.contains(vertexLabel)) {
			return;
		}

		else {
			path.add(block);
			checked.add(vertexLabel);
			Iterator<Entry<String, String>> iter = block.getTarget().entrySet()
					.iterator();
			while (iter.hasNext()) {
				// Iterate
				Map.Entry<String, String> pairs = (Map.Entry<String, String>) iter
						.next();
				String target_label = (String) pairs.getValue();

				Block block2 = reversed.getBlockFromLabel(target_label);
				if (block != null) {
					blockQueue.add(block2);
				}
			}
		}

	}

	// calls the recursive drawing method
	public static void drawGraphRecursive(Blocks matched, Blocks blocks,
			String classname, String pathname) {
		// for a group of blocks in the matched structure, generates all
		// possible paths
		// from it , given the blocks parameter which has the all the blocks in
		// the class
		GraphManager g = new GraphManager();
		HashSet<String> checked = new HashSet<String>();
		LinkedList<Block> blockQueue = new LinkedList<Block>();
		for (int i = 0; i < matched.blocks.size(); i++) {
			Block block = matched.blocks.get(i);
			blockQueue.add(block);
		}

		while (!blockQueue.isEmpty()) {
			drawRecursive(blockQueue, checked, g, blocks);
			blockQueue.remove();
		}
		g.draw(classname, pathname);

	}

	// draws the actual graph recursively
	public static void drawRecursive(LinkedList<Block> blockQueue,
			HashSet<String> checked, GraphManager g, Blocks blocks) {
		Block block = blockQueue.getFirst();

		String vertexLabel = block.getGraphNodeLabel();
		if (checked.contains(vertexLabel)) {
			return;
		}

		else {
			checked.add(vertexLabel);
			Iterator<Entry<String, String>> iter = block.getTarget().entrySet()
					.iterator();
			while (iter.hasNext()) {
				// Iterate
				Map.Entry<String, String> pairs = (Map.Entry<String, String>) iter
						.next();
				String edge_label = (String) pairs.getKey();
				if (edge_label.isEmpty())
					edge_label = "Cont";
				String target_label = (String) pairs.getValue();

				Block block2 = blocks.getBlockFromLabel(target_label);
				if (block2 != null) {
					blockQueue.add(block2);
					g.addEdge(block, block2, edge_label);
				}
			}
		}

	}

}
