import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Blocks {

	public ArrayList<Block> blocks;
	public HashMap<String, Integer> mp;
	public String className;
	public String classPath;

	public Blocks() {
		// HashMap of label to its position in ArrayList
		blocks = new ArrayList<Block>();
		mp = new HashMap<String, Integer>();
		className = null;
		classPath = null;

	}

	public Blocks(Blocks blks) {

		mp = new HashMap<String, Integer>();
		className = blks.className;
		classPath = blks.classPath;
		for (int i = 0; i < blks.blocks.size(); i++) {
			Block b = new Block(blks.blocks.get(i));
			this.blocks.add(b);
		}

	}

	// toString for printing graph
	public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(className + "\n");
		sb.append(classPath + "\n");

		for (int i = 0; i < this.blocks.size(); i++) {
			Block blk = this.blocks.get(i);
			sb.append(blk.getGraphNodeLabel() + "\n\n");
		}

		return sb.toString();
	}

	public void add(Block blk) {
		// Add block to the list if its non empty and not already present
		boolean isPresent = false;
		if (blk.getLength() > 0) {
			for (int i = 0; i < this.blocks.size(); i++) {
				// Checking for already existing blocks with labels
				String cur_label = this.blocks.get(i).getLabel();
				if (cur_label.equals(blk.getLabel())) {
					isPresent = true;
				}
			}

			if (!isPresent) {
				blocks.add(blk);
				mp.put(blk.getLabel(), blocks.size() - 1);
			}
		}
	}

	public Block addBefore(String label, String inst, Block blk, String pclass,
			String pmethod) {
		// Add the instruction to passed block and add block to current list and
		// returns new block
		blk.addInstruction(inst);
		this.add(blk);
		mp.put(blk.getLabel(), blocks.size() - 1);
		return new Block(pclass, pmethod, label, new ArrayList<String>());
	}

	public Block addAfter(String label, String inst, Block blk, String pclass,
			String pmethod) {
		// Add block to current list and return a new block with current
		// instructions
		this.add(blk);
		mp.put(blk.getLabel(), blocks.size() - 1);
		ArrayList<String> instructions = new ArrayList<String>();
		instructions.add(inst);
		return new Block(pclass, pmethod, label, instructions);
	}

	public static ArrayList<InstructionSet> extractBlocks(String filename) {
		// read raw file and save to variable
		String out_file = "";
		try {
			// Read file all at once (Not good for large files)
			File file = new File(filename);
			FileInputStream fis = new FileInputStream(file);
			byte[] data = new byte[(int) file.length()];
			fis.read(data);
			fis.close();
			out_file = new String(data, "UTF-8");
		} catch (IOException e) {
			System.out.println(filename + " doesnt exist \n");
			// e.printStackTrace();
			return null;
		}

		ArrayList<String> methods = new ArrayList<String>();
		// Search for method end and beginning using regex
		// escape the \ also to escape special characters

		// Extract class name from .class keyword
		// Matcher cname =
		// Pattern.compile("^\\/(.+\\/)*(.+)\\.(.+)$").matcher(filename);
		// http://www.regular-expressions.info/repeat.html
		// Extract classname in full with package name
		Matcher cname = Pattern.compile("^\\.class (.+ )*(.+?);").matcher(
				out_file);
		String class_name = null;
		if (cname.find()) {
			class_name = cname.group(2);
		}

		Pattern regex = Pattern.compile(
				"\\.method\\s(.*?)\\n(.*?)\\.end\\smethod", Pattern.DOTALL);
		Matcher m = regex.matcher(out_file);
		while (m.find()) {
			methods.add(m.group());
		}

		if (methods.isEmpty()) {
			return null;
		}

		else {
			ArrayList<InstructionSet> ret = new ArrayList<InstructionSet>();
			// for loop for all methods in class
			for (int i = 0; i < methods.size(); i++) {
				// convert one method into a list of instructions by splitting
				// string on new line
				String[] temp_arr = methods.get(i).split(
						System.getProperty("line.separator"));
				ArrayList<String> inst = new ArrayList<String>(
						Arrays.asList(temp_arr));

				Iterator<String> iter = inst.iterator();
				while (iter.hasNext()) {
					String curr = iter.next();
					Matcher linematch = Pattern.compile("^ +?\\.line .+")
							.matcher(curr);
					if (curr.isEmpty() || linematch.matches()) {
						// Empty or .line instructions
						// Use iterator to remove values as list is processed
						iter.remove();
					}
				}

				for (int j = 0; j < inst.size(); j++) {
					// Remove leading space with regex and replace in current
					// list
					inst.set(j, inst.get(j).replaceAll("^\\s+", ""));
				}

				// set method name as full method name with parameters and
				// return type
				// Extract method name from instructions
				String[] temp_arr2 = inst.get(0).split(" ");
				// String[] temp_arr3 = temp_arr2[temp_arr2.length - 1]
				// .split("\\(");
				// Use full method name for searching
				String method_name = temp_arr2[temp_arr2.length - 1];
				// remove the .method line
				inst.remove(0);
				ret.add(new InstructionSet(class_name, method_name, inst));
			}

			return ret;
		}

	}

	public static ArrayList<String> splitBlock(Block blk, String classn,
			String methodn, int pos, int lenInc, ArrayList<String> iset,
			String i) {
		// Split blocks into smaller pieces
		// next instruction for the block is number of instruction + current
		// position + length to increment
		int blockLen = blk.getLength() + lenInc;
		ArrayList<String> ret = new ArrayList<String>();
		String incrementalLabel = classn + " " + methodn + " "
				+ (blockLen + pos);
		ret.add(incrementalLabel);

		if (blk.isTag(i, "isCall")) {
			// Jump to to the next instruction after the invoke
			// Same as incrementalLabel but original authors way used below
			String temp_arr[] = blk.getLabel().split(" ");
			int lindex = Integer.parseInt(temp_arr[temp_arr.length - 1])
					+ blk.getLength();
			String positionalLabel = temp_arr[0] + " " + temp_arr[1] + " "
					+ ++lindex;
			ret.add(positionalLabel);
		}

		else if (blk.isTag(i, "isSwitchStart")) {
			String temp_arr[] = i.split(" ");
			String targetLabel = temp_arr[temp_arr.length - 1];
			// goto where the .switch starts
			Matcher m;
			Pattern startSwitch = Pattern.compile("^\\.(.+?)-switch(.*)");
			int jumpTo = iset.indexOf(targetLabel);

			m = startSwitch.matcher(iset.get(++jumpTo));

			while (!m.matches()) {
				m = startSwitch.matcher(iset.get(++jumpTo));
			}

			// while the .end switch not reach keep adding targets
			// keep incrementing counter
			Pattern endSwitch = Pattern.compile("^\\.end (.+?)switch");
			m = endSwitch.matcher(iset.get(++jumpTo));
			// while end not reached or the end of method is not one instruction
			// away
			while (!m.matches() && jumpTo < (iset.size() - 1)) {
				String positionalLabel = classn + " " + methodn + " "
						+ (jumpTo + 1);
				ret.add(positionalLabel);
				m = endSwitch.matcher(iset.get(++jumpTo));
			}
		}

		else if (blk.isSwitchJump(i, pos, iset)) {

			String temp_arr[] = i.split(" ");
			String targetLabel = temp_arr[temp_arr.length - 1];
			// indexof() works because the returned occurrence is the first one
			// and that is the one we need
			int jumpTo = iset.indexOf(targetLabel) + 1;
			String positionalLabel = classn + " " + methodn + " " + (jumpTo);
			ret.add(positionalLabel);
		}

		else if (blk.isTag(i, "isReturn")) {
			// if instruction is return then no target.
		}

		else if (blk.isTag(i, "isTryStart")) {
			// branch at try start
			int index;

			for (index = 0; index < iset.size(); index++) {
				// search for where the jump to catch occurs at {try_start ..
				// try_end}
				Matcher m = Pattern.compile(
						"^.+?\\{\\:try_start.+?\\.\\. :try_end.+\\}.+")
						.matcher(iset.get(index));
				if (m.matches()) {
					break;
				}
			}

			String temp_arr[] = iset.get(index).split(" ");
			String targetLabel = temp_arr[temp_arr.length - 1];
			int jumpTo = iset.indexOf(targetLabel) + 1;
			// int jumpTo = index + 2;//jump to statement after the try_start_XX
			// .. try_end
			String positionalLabel = classn + " " + methodn + " " + (jumpTo);
			ret.add(positionalLabel);
		}

		else {
			// for not is call, check the ":---" labels
			// search instructions string for the label and jump to that
			// indexloc
			// the jump location will be in the same method
			String temp_arr[] = i.split(" ");
			String targetLabel = temp_arr[temp_arr.length - 1];
			int jumpTo = iset.indexOf(targetLabel) + 1;
			String positionalLabel = classn + " " + methodn + " " + (jumpTo);
			ret.add(positionalLabel);
		}

		return ret;
	}

	public Block getBlockFromLabel(String label) {
		if (this.mp.containsKey(label)) {
			int target_index = this.mp.get(label);
			Block block = this.blocks.get(target_index);
			return block;
		}
		return null;
	}
}
