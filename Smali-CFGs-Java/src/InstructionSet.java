import java.util.ArrayList;

public class InstructionSet {
	// Return type object of extract blocks
	public String class_name;
	public String method_name;
	public ArrayList<String> instructions;

	public InstructionSet() {

		class_name = null;
		method_name = null;
		instructions = new ArrayList<String>();
	}

	public InstructionSet(String class_name, String method_name,
			ArrayList<String> inst) {
		// Constructor
		this.class_name = class_name;
		this.method_name = method_name;
		this.instructions = inst;
	}
}
