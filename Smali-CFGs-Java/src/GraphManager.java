import java.io.File;

public class GraphManager {
	// Uses graphviz api from :
	// http://www.loria.fr/~szathmar/off/projects/java/GraphVizAPI/index.php
	public GraphViz gv = new GraphViz();

	public GraphManager() {
		this.gv.addln(gv.start_graph());
	}

	public void addEdge(Block b1, Block b2, String edge_label) {
		// All inputs go to the dot package installed on the machine
		gv.addln("\"" + b1.getLabel() + "\"" + " -> " + "\"" + b2.getLabel()
				+ "\" [label = \"" + edge_label + "\"] ;");
		gv.addln("\"" + b1.getLabel() + "\"" + " [label = \""
				+ b1.getGraphNodeLabel() + "\"];");
		gv.addln("\"" + b2.getLabel() + "\"" + " [label = \""
				+ b2.getGraphNodeLabel() + "\"];");

	}

	public void addNode(Block b) {
		gv.addln("\"" + b.getLabel() + "\"" + " [label = \""
				+ b.getGraphNodeLabel() + "\"];");
	}

	public void draw(String filename, String pathname) {
		// replacing the "/" which causes problems in outputting files with "|"
		String fil = filename.replace("/", "|");
		System.out.println(fil);
		gv.addln(gv.end_graph());
		// System.out.println(gv.getDotSource());
		String type = "gif";
		File out = new File(pathname + fil + "." + type);
		gv.writeGraphToFile(gv.getGraph(gv.getDotSource(), type), out);
	}
}
