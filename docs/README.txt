Objectives:

1) Build the control flow graph for one class

2) Build the call graph for the whole project
  (Call methods: Composition, Intent, Messenger, etc.)
